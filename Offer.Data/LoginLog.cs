//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Offer.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class LoginLog
    {
        public int LoginLogId { get; set; }
        public int UserId { get; set; }
        public System.DateTime DateCreated { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
