//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Offer.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class Organization
    {
        public Organization()
        {
            this.EmailTemplates = new HashSet<EmailTemplate>();
            this.TransactionLedgers = new HashSet<TransactionLedger>();
        }
    
        public int OrganizationID { get; set; }
        public string Organization1 { get; set; }
        public System.DateTime DateCreated { get; set; }
        public System.DateTime DateModified { get; set; }
        public Nullable<int> CreatedByUserId { get; set; }
        public Nullable<int> ModifiedByUserId { get; set; }
        public string Payment_NameOnCard { get; set; }
        public Nullable<int> Payment_ExpirationMonth { get; set; }
        public Nullable<int> Payment_ExpirationYear { get; set; }
        public string Payment_Token { get; set; }
        public string Payment_Last4Digits { get; set; }
        public string Payment_CardType_LookupCode { get; set; }
        public string Payment_CardNumber { get; set; }
        public string OrganizationType_LookupCode { get; set; }
        public Nullable<int> ParentOrganization { get; set; }
    
        public virtual ICollection<EmailTemplate> EmailTemplates { get; set; }
        public virtual ICollection<TransactionLedger> TransactionLedgers { get; set; }
    }
}
