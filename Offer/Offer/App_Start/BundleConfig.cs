﻿using System.Web;
using System.Web.Optimization;

namespace Offer
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/plugins").Include(
                "~/Scripts/jquery.iframe-transport.js").Include(
                "~/Scripts/jquery.ui.widget.js", 
                "~/Components/dataTables/js/jquery.dataTables.js",
                "~/Component/dataTables/js/dataTables.bootstrap.js",
                "~/Component/metisMenu/metisMenu.js",
                "~/Component/wow/wow.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Component/dataTables/css/dataTables.bootstrap.css",
                      "~/Component/metisMenu/metisMenu.css",
                      "~/Component/font-awesome/css/font-awesome.css",
                      "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/styles/styles.css"));

            bundles.Add(new ScriptBundle("~/bundles/jquery-file-upload").Include(
                "~/Component/jQuery-File-Upload/js/jquery.fileupload.js"));//.Include(
                //"~/Scripts/Plugins/jQuery-File-Upload/js/jquery.fileupload-audio.js").Include(
                //"~/Scripts/Plugins/jQuery-File-Upload/js/jquery.fileupload-process.js").Include(
                //"~/Scripts/Plugins/jQuery-File-Upload/js/jquery.fileupload-validate.js").Include(
                //"~/Scripts/Plugins/jQuery-File-Upload/js/load-image.all.min.js").Include(
                //"~/Scripts/Plugins/jQuery-File-Upload/js/jquery.fileupload-image.js").Include(
                //"~/Scripts/Plugins/jQuery-File-Upload/js/jquery.fileupload-ui.js"));
                
            //bundles.Add(new StyleBundle("~/bundles/jquery-file-upload-css").IncludeDirectory(
            //    "~/Component/jQuery-File-Upload/css", "*.css"));

            bundles.Add(new ScriptBundle("~/bundles/js").Include(
                "~/Scripts/core.js"));
        }
       
    }
}
