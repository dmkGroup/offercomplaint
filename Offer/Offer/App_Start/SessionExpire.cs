﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Offer.App_Start
{
    public class SessionExpire : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpContext ctx = HttpContext.Current;
            HttpCookie langCookie1 = HttpContext.Current.Request.Cookies["UserId"];
            HttpCookie langCookie2 = HttpContext.Current.Request.Cookies["FirstName"];
            HttpCookie langCookie3 = HttpContext.Current.Request.Cookies["LastName"];
         
            if (langCookie1 != null)
            {
                HttpContext.Current.Session["UserId"] = langCookie1.Value;
            }
            if (langCookie2 != null)
            {
                HttpContext.Current.Session["FirstName"] = langCookie2.Value;
            }
            if (langCookie3 != null)
            {
                HttpContext.Current.Session["LastName"] = langCookie3.Value;
            }
          
            // check  sessions here
            if (HttpContext.Current.Session["UserId"] == null)
            {
                filterContext.Result = new RedirectResult("../Account/Login");
                return;
            }
            base.OnActionExecuting(filterContext);
        }
    }
    
}