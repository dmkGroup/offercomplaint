﻿using Offer.Data;
using Offer.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
//using System.Net.Mail;
using System.Web.Http;
namespace Offer.Api
{
    public class OfferApiController : ApiController
    {

        public void SendEmail(string subject, string message, string to)
        {
            var FromEmailid = "dmkglori@gmail.com";
            var Pass = "dmkglori123";
            System.Net.Mail.MailMessage mailMessage = new System.Net.Mail.MailMessage();
            mailMessage.From = new System.Net.Mail.MailAddress(FromEmailid);
            mailMessage.Subject = subject;
            mailMessage.Body = message;
            mailMessage.IsBodyHtml = true;
            mailMessage.To.Add(new System.Net.Mail.MailAddress(to));
            System.Net.Mail.SmtpClient smpt = new System.Net.Mail.SmtpClient();
            smpt.Host = "smtp.gmail.com";
            smpt.EnableSsl = true;
            NetworkCredential Networkcred = new NetworkCredential();
            Networkcred.UserName = mailMessage.From.Address;
            Networkcred.Password = Pass;
            smpt.UseDefaultCredentials = false;
            smpt.Credentials = Networkcred;
            smpt.Port = 587;
            smpt.Send(mailMessage);
        }
        AttachmentLogic attchlogic = new AttachmentLogic();
        BuyerLogic buyerlogic = new BuyerLogic();
        ListingLogic lslogic = new ListingLogic();
        LookUpLogic lkuplogic = new LookUpLogic();
        OfferLogic offerlogic = new OfferLogic();
        SellerLogic sellerlogic = new SellerLogic();
        UserLogic userlogic = new UserLogic();
        #region Attachment
        public void SaveAttachment(Attachment model)
        {
            attchlogic.SaveAttachment(model);
        }
        public void EditAttachment(Attachment model)
        {
            attchlogic.EditAttachment(model);
        }
        public List<Attachment> GetAttchment()
        {
            return attchlogic.GetAttchment();
        }
        public Attachment GetListingAttachment(int ListingId)
        {
            return attchlogic.GetListingAttachment(ListingId);
        }
        public List<Attachment> GetOfferAttachment(int OfferId)
        {
            return attchlogic.GetOfferAttachment(OfferId);
        }
        public Attachment GetProfileAttachment(int UserId)
        {
            return attchlogic.GetProfileAttachment(UserId);
        }
        #endregion
        #region Buyer
        public void BuyerGroupSave(BuyerGroup buyerGroup)
        {
            buyerlogic.BuyerGroupSave(buyerGroup);
        }
        public List<BuyerGroup> GetAllBuyerGroup()
        {
            return buyerlogic.GetAllBuyerGroup();
        }
        public BuyerGroup GetBuyerGroupDetail(int BuyerGroupId)
        {
            return buyerlogic.GetBuyerGroupDetail(BuyerGroupId);
        }
        public void EditBuyerGroup(BuyerGroup BuyerGroup)
        {
            buyerlogic.EditBuyerGroup(BuyerGroup);
        }
        public List<Buyer> GetAllBuyerList()
        {
            return buyerlogic.GetAllBuyerList();
        }
        public Buyer GetBuyerDetail(int id)
        {
            return buyerlogic.GetBuyerDetail(id);
        }
        public List<Buyer> GetBuyerByGroupId(int BuyerGroupId)
        {
            return buyerlogic.GetBuyerByGroupId(BuyerGroupId);
        }
        public void SaveBuyer(Buyer Buyer)
        {
             buyerlogic.SaveBuyer(Buyer);
        }
        public void SaveBuyerGroup(BuyerGroup Buyer)
        {
            buyerlogic.SaveBuyerGroup(Buyer);
        }
        
        public Buyer GetBuyerByEmail(string Email)
        {
            return buyerlogic.GetBuyerByEmail(Email);
        }
        public List<Buyer> GetBuyerByName(string Name)
        {
            return buyerlogic.GetBuyerByName(Name);
        }
        public void EditBuyer(Buyer Buyer)
        {
            buyerlogic.EditBuyer(Buyer);
        }
        public void DeleteBuyer(int BuyerId)
        {
            buyerlogic.DeleteBuyer(BuyerId);
        }
        public List<Buyer> GetBuyersByListing(int ListingId)
        {
            return buyerlogic.GetBuyersByListing(ListingId);
        }
        #endregion
        #region Listing
        public List<Listing> SearchListings()
        {
            return lslogic.SearchListings();
        }
        public List<Listing> ListingByPrefix()
        {
            return lslogic.ListingByPrefix();
        }
        public Listing GetListingById(int id)
        {
            return lslogic.GetListingById(id);
        }
        public Listing GetListingByUniqueId(string uniqueId)
        {
            return lslogic.GetListingByUniqueId(uniqueId);
        }
        public List<Listing> GetListingByAddress(string Address)
        {
            return lslogic.GetListingByAddress(Address);
        }
        public List<Listing> GetListingByStatus(string Status)
        {
            return lslogic.GetListingByStatus(Status);
        }
        public void ChangeListingStatus(int Id, string Status)
        {
            lslogic.ChangeListingStatus(Id, Status);
        }
        public int SaveListing(Listing Model)
        {

            return lslogic.SaveListing(Model);
        }
        public void EditListing(Listing Model)
        {
             lslogic.EditListing(Model);
        }
        #endregion
        #region LookUp
        public List<lookup_ListingStatus> GetListingStatus()
        {
            return lkuplogic.GetListingStatus();
        }
        public List<lookup_LoanType> GetLoanTypeStatus()
        {
            return lkuplogic.GetLoanTypeStatus();
        }
        public List<lookup_OfferStatus> GetOfferStatus()
        {
            return lkuplogic.GetOfferStatus();
        }
        public List<lookup_AttachmentType> GetAttachmentType()
        {
            return lkuplogic.GetAttachmentType();
        }
        public lookup_AttachmentType GetAttachmentTypeDetail(int Id)
        {
            return lkuplogic.GetAttachmentTypeDetail(Id);
        }
        public List<lookup_BusinessType> GetBusinessType()
        {
            return lkuplogic.GetBusinessType();
        }
        public lookup_BusinessType GetBusinessTypeDetail(int Id)
        {
            return lkuplogic.GetBusinessTypeDetail(Id);
        }
        public List<geo_State> GetAllStates()
        {
            return lkuplogic.GetAllStates();
        }
        #endregion
        #region Offer
        public List<Offer.Data.Offer> GetAllOffers()
        {
            return offerlogic.GetAllOffers();
        }
        public Offer.Data.Offer GetOfferById(int id)
        {
            return offerlogic.GetOfferById(id);
        }
        public void SaveOffer(Offer.Data.Offer Offer)
        {
            offerlogic.SaveOffer(Offer);
        }
        public List<Offer.Data.Offer> GetOfferByStatus(string Status)
        {
            return offerlogic.GetOfferByStatus(Status);
        }
        public void ChangeStatus(int Id, string Status)
        {
            offerlogic.ChangeStatus(Id, Status);
        }
        public List<OfferGroupSummary> OfferGroupSummarySearch(int listingId)
        {
            return offerlogic.OfferGroupSummarySearch(listingId);
        }
        public List<OfferGroupSummary> BuyerGroupOffers(int BuyerGroupId)
        {
            return offerlogic.BuyerGroupOffers(BuyerGroupId);
        }
        public void OfferGroupSave(OfferGroup offerGroup)
        {
            offerlogic.OfferGroupSave(offerGroup);
        }
        public List<OfferGroup> GetAllOfferGroup()
        {
            return offerlogic.GetAllOfferGroup();
        }
        public OfferGroup GetOfferGroupDetail(int OfferGroupId)
        {
            return offerlogic.GetOfferGroupDetail(OfferGroupId);
        }
        public List<OfferGroupSummary> GetOfferGroupByUniqueId(Guid UniqueId)
        {
            return offerlogic.GetOfferGroupByUniqueId(UniqueId);
        }
        public List<OfferGroup> GetOfferGroupByListing(int ListingId)
        {
            return offerlogic.GetOfferGroupByListing(ListingId);
        }
        public List<Offer.Data.Offer> GetOfferByGroup(int Id)
        {
            return offerlogic.GetOfferByGroup(Id);
        }
        public Offer.Data.Offer GetOfferDetail(int Id)
        {
            return offerlogic.GetOfferDetail(Id);
        }
        public void ChangeOfferStatus(int Id, string Status)
        {
            offerlogic.ChangeOfferStatus(Id, Status);
        }
        public void EditOfferNote(Offer.Data.Offer offer)
        {
            offerlogic.EditOfferNote(offer);
        }
        #endregion
        #region Seller
        public List<Seller> GetAllSellerList()
        {
            return sellerlogic.GetAllSellerList();
        }
        public Seller GetSellerDetail(int id)
        {
            return sellerlogic.GetSellerDetail(id);
        }
        public void SaveSeller(Seller Seller)
        {
            sellerlogic.SaveSeller(Seller);
        }
        public Seller GetSellerByEmail(string Email)
        {
            return sellerlogic.GetSellerByEmail(Email);
        }
        public void EditSeller(Seller Seller)
        {
            sellerlogic.EditSeller(Seller);
        }
        public void DeleteSeller(int SellerId)
        {
            sellerlogic.DeleteSeller(SellerId);
        }
        public List<Seller> GetSellerByListing(int ListingId)
        {
            return sellerlogic.GetSellerByListing(ListingId);
        }
        public List<Seller> GetSellerByName(string Name)
        {
            return sellerlogic.GetSellerByName(Name);
        }
        #endregion
        #region User
        public List<User> GetAllUserList()
        {
            return userlogic.GetAllUserList();
        }
        public User GetUserDetail(int id)
        {
            return userlogic.GetUserDetail(id);
        }
        public void SaveUser(User User)
        {
            userlogic.SaveUser(User);
        }
        public User GetUserByEmail(string Email)
        {
            return userlogic.GetUserByEmail(Email);
        }
        public User GetUserByEmail_Password(string Email, string Password)
        {
            return userlogic.GetUserByEmail_Password(Email, Password);
        }
        
        public List<User> GetUserByStatus(bool Status)
        {
            return userlogic.GetUserByStatus(Status);
        }
        public void ChangeStatus(int Id, bool Status)
        {
            userlogic.ChangeStatus(Id, Status);
        }
        public void ChangePassword(int Id, string Password)
        {
            userlogic.ChangePassword(Id, Password);
        }
        public void EditUser(User user)
        {
            userlogic.EditUser(user);
        }
        public User GetUserByToken(string Token)
        {
            return userlogic.GetUserByToken(Token);
        }
        #endregion
    }
}