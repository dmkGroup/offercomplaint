﻿using Microsoft.Owin;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;

namespace Offer.Core
{
    //todo: move this to DubTech.Infrastructure and replace DubFormsAuth
    public static class Authentication
    {
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public static void Authenticate(string username, string password)
        {
            
        }
        public static void SignIn(string name, string nameId, int? organizationId, string firstname, string lastname, IOwinContext ctx, bool isPersistent)
        {
            var identity = new ClaimsIdentity(new[] {
                new Claim(ClaimTypes.Name, name),
                new Claim(ClaimTypes.NameIdentifier, nameId),
                new Claim(ClaimTypes.PrimaryGroupSid, organizationId.ToString()),
                new Claim(ClaimTypes.GivenName, firstname),
                new Claim(ClaimTypes.Surname, lastname)
            },
                    Microsoft.AspNet.Identity.DefaultAuthenticationTypes.ApplicationCookie);

            var authManager = ctx.Authentication;

            if (isPersistent)
            {
                authManager.SignIn(new AuthenticationProperties() { IsPersistent = true }, identity);
            }
            else
            {
                authManager.SignIn(new AuthenticationProperties() { ExpiresUtc = new DateTimeOffset(DateTime.UtcNow.AddMinutes(30)) }, identity);
            }
        }

        public static void Logout(IOwinContext ctx)
        {
            ctx.Authentication.SignOut(Microsoft.AspNet.Identity.DefaultAuthenticationTypes.ApplicationCookie); 
        }
    }
}