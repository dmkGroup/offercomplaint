﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;

namespace Offer.Core
{
    public class AppUser : ClaimsPrincipal
    {
        public AppUser(ClaimsPrincipal principal)
            : base(principal)
        {

        }

        public string Name { get {  return this.FindFirst(ClaimTypes.Name).Value;} }
        public string NameIdentifier { get { return this.FindFirst(ClaimTypes.NameIdentifier).Value; } }

    }
}