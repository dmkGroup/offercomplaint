﻿$(function() {

$('#NewOffer').on('submit', 'form', function (e) {
    e.preventDefault();
    submitNewOffer();
});

});


function submitNewOffer() {
    var url = "/Offer/NewOffer"; // the script where you handle the form input.

    $.ajax({
        type: "POST",
        url: url,
        data: $("#newOfferForm").serialize(), // serializes the form's elements.
        success: function (data) {
            alert(data); // show response from the php script.
        }
    });
}