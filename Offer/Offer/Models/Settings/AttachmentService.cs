﻿using Offer.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace Offer.Models.Settings
{
    public  class AttachmentService
    {
       

      
        #region Temporary Files

        /// <summary>
        /// Saves file to temporary storage
        /// </summary>
        /// <returns>The filename (guid) of the temporary file created</returns>
        public  string SaveTemporaryFile(byte[] file)
        {
            string filename = Guid.NewGuid().ToString();
            string tempFilePath = Path.Combine(Config.TemporaryFilePath, filename);
            File.WriteAllBytes(tempFilePath, file);
            return filename;
        }

        /// <summary>
        /// Saves file to temporary storage
        /// </summary>
        /// <returns>The filename (guid) of the temporary file created</returns>
        public  string SaveTemporaryFile(Stream stream)
        {
            stream.Position = 0;
            string filename = Guid.NewGuid().ToString();
            string tempFilePath = Path.Combine(Config.TemporaryFilePath, filename);
            using (FileStream fs = File.Create(tempFilePath))
            {
                stream.CopyTo(fs);
            }

            return filename;
        }


        /// <summary>
        /// Load temporary file
        /// </summary>
        public  byte[] GetTemporaryFile(string filename)
        {
            string tempFilePath = Path.Combine(Config.TemporaryFilePath, filename);
            return File.ReadAllBytes(tempFilePath);
        }

        #endregion


        #region Path Resolution Helpers

        /// <summary>
        /// Get calculated path location: [root]\ (attachmentId divided by 100000), eg attachmentid 120000 goes to [root]\12
        /// </summary>
        public static string GetAttachmentFullPath(Attachment attachment, bool createDirectoryIfMissing, string attachmentRootPath)
        {
            try
            {
                int destinationNumber = (int)Math.Floor((double)attachment.AttachmentId / 100000);
                string path = Path.Combine(attachmentRootPath, destinationNumber.ToString());

                //verify path exists and create if needed
                if (createDirectoryIfMissing)
                    if (!Directory.Exists(path)) Directory.CreateDirectory(path);

                return Path.Combine(path, attachment.LocalFilename);
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to determine attachment path for attachment id: " + attachment.AttachmentId.ToString(), ex);
            }
        }

        public static string GetAttachmentFullPath(Attachment attachment, bool createDirectoryIfMissing)
        {
            return GetAttachmentFullPath(attachment, createDirectoryIfMissing, Config.AttachmentRootPath);
        }

        #endregion
    }
}