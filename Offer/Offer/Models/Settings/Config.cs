﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Offer.Models.Settings
{
    
         public  class Config
    {
        public static string ApplicationName
        {
            get
            {
                return ConfigurationManager.AppSettings["ApplicationName"];
            }
        }

        public static string TemporaryFilePath
        {
            get
            {
                return ConfigurationManager.AppSettings["TemporaryFilePath"];
            }
        }

        public static string AttachmentRootPath
        {
            get
            {
                return ConfigurationManager.AppSettings["AttachmentRootPath"];
            }
        }

    
}
     
}