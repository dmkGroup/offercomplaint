﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace Offer.Models.Settings
{
    public class FormatHelper
    {
        public static string FormatFullName(string firstName, string lastName)
        {
            return string.Join(" ", firstName, lastName).Trim();
        }

        /// <summary>
        /// phone string needs exactly 10 digits before optional 'x', if there is an 'x' then must be at least 1 maximum 10 digits after the 'x'
        /// e.g.: (555) 555.5555 x 123 is valid
        /// </summary>
        /// <param name="phone"></param>
        /// <returns></returns>
        public static bool TryFlattenPhone(ref string phone)
        {
            string flattenedPhone = FlattenPhone(phone);
            if (!String.IsNullOrWhiteSpace(flattenedPhone))
            {
                phone = flattenedPhone;
                return true;
            }
            return false;
        }

        public static string FlattenPhone(string phone)
        {
            string flattenedPhone = null;

            if (!String.IsNullOrWhiteSpace(phone))
            {
                string flatPhone = String.Empty;
                string flatExtension = String.Empty;

                string[] phoneArray = phone.ToLower().Split('x');
                flatPhone = String.Concat(from c in phoneArray[0] where char.IsDigit(c) select c);
                if (flatPhone != null && flatPhone.Length == 10)
                {
                    if (phoneArray.Length == 1)
                    {
                        flattenedPhone = flatPhone;
                    }
                    else
                    {
                        flatExtension = String.Concat(from c in phoneArray[1] where char.IsDigit(c) select c);
                        if (flatExtension != null && flatExtension.Length > 0 && flatExtension.Length <= 10)
                        {
                            if (!String.IsNullOrWhiteSpace(flatExtension)) flatExtension = "x" + flatExtension;
                            flattenedPhone = flatPhone + flatExtension.Trim();
                        }
                    }
                }
            }

            return flattenedPhone;
        }

        public static string FormatPhoneForDisplay(string phone)
        {
            if (TryFlattenPhone(ref phone))
            {
                string phoneOnly = String.Empty;
                string extenstionPart = String.Empty;

                string[] phoneArray = phone.Split('x');
                phoneOnly = Regex.Replace(phoneArray[0], @"(\d{3})(\d{3})(\d{4})", "($1) $2-$3");
                if (phoneArray.Length > 1)
                {
                    extenstionPart = " x" + phoneArray[1];
                }

                return phoneOnly + extenstionPart;
            }

            return phone;
        }

        public static string FormatFullStreetAddress(string address1, string address2)
        {
            if (!string.IsNullOrWhiteSpace(address1) && !string.IsNullOrWhiteSpace(address2))
                return string.Join(", ", address1.Trim(), address2.Trim());
            else if (!string.IsNullOrWhiteSpace(address1))
                return address1.Trim();
            else if (!string.IsNullOrWhiteSpace(address2))
                return address2.Trim();
            else return null;
        }

        public static string FormatAddressCityState(string address, string city, string state)
        {
            var res = string.Format("{0}, {1}, {2}",
                address,
                city,
                state).Trim();
            return res;
        }

        public static string FormatCityStateZip(string city, string state, string zip, bool normalizeCase = false)
        {
            if (normalizeCase)
            {
                CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
                TextInfo textInfo = cultureInfo.TextInfo;

                if (state != null)
                    state = state.ToUpper(cultureInfo);

                if (city != null)
                    city = textInfo.ToTitleCase(city);
            }

            return String.Format("{0}, {1} {2}", city, state, zip);
        }

        public static string FormatAddressCityStateZip(string address, string city, string state, string zip, bool normalizeCase = false)
        {
            string toTitleCase = string.Format("{0}, {1}", address, city).Trim();
            string toUpperCase = string.Format(", {0} {1}", state, zip).Trim();

            if (normalizeCase)
            {
                CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
                TextInfo textInfo = cultureInfo.TextInfo;
                toTitleCase = string.IsNullOrWhiteSpace(toTitleCase) ? toTitleCase : textInfo.ToTitleCase(toTitleCase);

                toUpperCase = toUpperCase.ToUpper();
            }

            return string.Format("{0}{1}", toTitleCase, toUpperCase).Trim(new char[] { ' ', ',' });
        }

        public static string FormatAddressCityStateZip(string address1, string address2, string city, string state, string zip, bool normalizeCase = false)
        {
            return FormatAddressCityStateZip(FormatFullStreetAddress(address1, address2), city, state, zip, normalizeCase);
        }

        public static string FormatCityStateZip(string city, string state, string zip)
        {
            var res = string.Format("{0}{1} {2} {3}",
                city,
                string.IsNullOrWhiteSpace(city) || (string.IsNullOrWhiteSpace(state) && string.IsNullOrWhiteSpace(zip)) ? null : ",",
                state,
                zip).Trim();
            return res;
        }

        public static string FormatFileSizeBytesForDisplay(long fileSize)
        {
            decimal tempSize;
            string suffix;

            if (fileSize > 1048576)
            {
                tempSize = Math.Round((decimal)fileSize / 1048576, 2);
                suffix = "MB";
            }
            else if (fileSize > 1024)
            {
                tempSize = Math.Round((decimal)fileSize / 1024, 0);
                suffix = "KB";
            }
            else
            {
                tempSize = (decimal)fileSize;
                suffix = "bytes";
            }

            return String.Format("{0} {1}", tempSize.ToString(), suffix);
        }

        public static string FormatDecimalPlace(decimal? number)
        {
            if (number.HasValue)
            {
                string s = string.Format("{0:0.00}", number);

                if (s.EndsWith("00"))
                    return ((int)number).ToString();
                else
                    return s;
            }
            else return "";
        }

        public static string FormatDecimalPlace(double? number)
        {
            if (number.HasValue)
            {
                string s = string.Format("{0:0.00}", number);

                if (s.EndsWith("00"))
                    return ((int)number).ToString();
                else
                    return s;
            }
            else
                return "";
        }

        [Obsolete("Should use Path.GetFileNameWithoutExtension")]
        public static string GetFileNameFromFile(string input, string character)
        {
            int str = input.LastIndexOf(character, StringComparison.Ordinal);
            return input.Substring(0, str);
        }

        public static string GetClassName(string displayName)
        {
            switch (displayName)
            {
                case "Rejected":
                    return "RejectedStatus";
                case "Countered":
                    return "CounteredStatus";
                case "Pending":
                    return "PendingStatus";
                case "Accepted":
                    return "AcceptedStatus";
                case "Canceled":
                    return "CanceledStatus";
            }
            return "NewStatus";
        }


        /// <summary>
        /// Returns a string with only the numbers from the input
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string JustNumbers(string input)
        {
            string justNumbers = new String(input.Where(Char.IsDigit).ToArray());
            return justNumbers;
        }
    }
}
