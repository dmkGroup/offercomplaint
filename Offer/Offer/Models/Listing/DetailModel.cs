﻿
using Offer.Models.Offer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Offer.Models.Listing
{
    public class DetailModel
    {
        public int ListingId { get; set; }
        public string ListingAgent { get; set; }
        public string AgentEmail { get; set; }
        public string AgentPhone { get; set; }
        public string ListingStatus { get; set; }
        public string ListingStatusCode { get; set; }
        public string StreetAddress { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string PostalCode { get; set; }

        public string FormattedAddress { get; set; }

        public int ListPrice { get; set; }

        public string FormattedListPrice { get; set; }

        public DateTime? ListDate { get; set; }

        public DateTime? ExpirationDate { get; set; }
        public string MlsUrl { get; set; }

        public string PublicUrl { get; set; }

        public string FormattedListDate { get; set; }
        public string UniqueId { get; set; }
        public string OfferInstructions { get; set; }
        public OfferGroupSearchModel OfferGroups { get; set; }
        public byte[] PropertyImage { get; set; }
    }
}