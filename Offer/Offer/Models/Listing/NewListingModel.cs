﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Offer.Models.Listing
{
    public class NewListingModel
    {
        public string Address { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string PostalCode { get; set; }

        public string MlsNumber { get; set; }

        public string SellerFirstname { get; set; }
        public string SellerLastname { get; set; }
        public string SellerPhone { get; set; }
        public string SellerEmail { get; set; }
        public string SellerAddress { get; set; }
        public string SellerCity { get; set; }
        public string SellerState { get; set; }
        public string SellerPincode { get; set; }

        public DateTime? ListDate { get; set; }
        public int? ListAmount { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public string OfferInstruction { get; set; }
    }
}