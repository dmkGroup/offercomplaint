﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Offer.Models.Offer;

namespace Offer.Models.Listing
{
    public class SearchModel
    {
        public List<ListingGridItem> ListingItems { get; set; }
    }

    public class ListingGridItem
    {
        public int ListingId { get; set; }
        public string Address { get; set; }
        public int ListAmount { get; set; }
        public DateTime? ListDate { get; set; }
        public string SellerNames { get; set; }
    //Newly Added Fields
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string MlsNumber { get; set; }
        public string SellerFirstName { get; set; }
        public string SellerLastName { get; set; }
        public string Status_LookupCode { get; set; }
        public bool IsDeleted { get; set; }       
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }
        public OfferGroupSearchModel OfferGroups { get; set; }
        public byte[] PropertyImage { get; set; }

    }
}