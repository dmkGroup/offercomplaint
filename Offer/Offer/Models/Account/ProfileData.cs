﻿using System.ComponentModel.DataAnnotations;


using Offer.Data;

namespace Offer.Models.Account
{
    public class ProfileData: User    {

       
       
        [Required(ErrorMessage = "Password is required")]
        public string Password { get; set; }
        [Compare("Password")]
        [StringLength(10, ErrorMessage = "Must be between 5 and 10 characters", MinimumLength = 5)]
        [Required(ErrorMessage = "Confirm Password is required")]
        public string ConfirmPassword { get; set; }
        public string Salt { get; set; }
        [Required(ErrorMessage = "E-mail Id is required")]
        [DataType(DataType.EmailAddress, ErrorMessage = "E-mail is not valid")]
        public string Email { get; set; }
         [Required(ErrorMessage = "First Name is required")]
        public string Firstname { get; set; }
         [Required(ErrorMessage = "Last Name is required")]
        public string Lastname { get; set; }
         [Required(ErrorMessage = "Phone Number is required")]
        public string PhoneNumber { get; set; }
        public string CompanyName { get; set; }
        public string LicenseNumber { get; set; }
      

    }
    public class ChangePasswordModel : ProfileData
    {
        [Required(ErrorMessage = "Password is required")]
        //[StringLength(10, ErrorMessage = "Must be between 5 and 10 characters", MinimumLength = 5)]
        [DataType(DataType.Password)]
        public string OldPassword { get; set; }

        [Required(ErrorMessage = "New Password is required")]
        [DataType(DataType.Password)]
       [StringLength(10, ErrorMessage = "Must be between 5 and 10 characters", MinimumLength = 5)]
        public string NewPassword { get; set; }

        [Required(ErrorMessage = "Confirm New Password is required")]
        [DataType(DataType.Password)]
        [Compare("NewPassword")]       
        //[StringLength(10, ErrorMessage = "Must be between 5 and 10 characters", MinimumLength = 5)]
        public string ConfirmNewPassword { get; set; }
    }
    public class ForgotPasswordModel
    {
        [Required(ErrorMessage = "E-mail Id is required")]
        [DataType(DataType.EmailAddress, ErrorMessage = "E-mail is not valid")]
        [EmailAddress]
        public string Email { get; set; }
    }
    public class ResetPasswordModel
    {      
        [Required(ErrorMessage = "New Password is required")]
        [DataType(DataType.Password)]
        [StringLength(10, ErrorMessage = "Must be between 5 and 10 characters", MinimumLength = 5)]
        public string NewPassword { get; set; }
        [Required(ErrorMessage = "Confirm New Password is required")]
        [DataType(DataType.Password)]
        [Compare("NewPassword")]
        public string ConfirmNewPassword { get; set; }

        public string PasswordResetToken { get; set; }
    }
}