﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Offer.Models.Register
{
    public class BillingView
    {
        public string CreditCardNumber { get; set; }

        public int ExpirationMonth { get; set; }
        
        public int ExpirationYear { get; set; }

        public string NameOnCard { get; set; }

        public string CardType { get; set; }

        public Guid RegistrationCode { get; set; }
    }
}