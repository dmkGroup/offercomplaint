﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Offer.Models.Offer
{
    public class NewOfferModel 
    {
        public DateTime? OfferDate { get; set; }
        public DateTime? ExpirationDate { get; set; }
        [Required(ErrorMessage = "Please Enter Offer Amount")]
        [Display(Name = "OfferAmount")]
        [RegularExpression(@"^[+-]?\$?([0-9]+(,[0-9]{3})*)?(\.([0-9]+)?)?$",
        ErrorMessage = "Please Enter Proper Amount.")]
        public int? OfferAmount { get; set; }
        public string LoanType { get; set; }
        public DateTime? ClosingDate { get; set; }
        [Required(ErrorMessage = "Please Enter Deposit Amount")]
        [Display(Name = "DepositAmount")]
        [RegularExpression(@"^[+-]?\$?([0-9]+(,[0-9]{3})*)?(\.([0-9]+)?)?$",
        ErrorMessage = "Please Enter Proper Amount.")]
        public decimal DepositAmount { get; set; }
        public string Status { get; set; }
        [Required(ErrorMessage = "Please Enter Loan Amount")]
        [Display(Name = "LoanAmount")]
        [RegularExpression(@"^[+-]?\$?([0-9]+(,[0-9]{3})*)?(\.([0-9]+)?)?$",
        ErrorMessage = "Please Enter Proper Amount.")]
        public decimal LoanAmount { get; set; }
        public string AgentEmail { get; set; }
        [Required(ErrorMessage = "Please Enter Buyer's Closing Cost")]
        [Display(Name = "BuyersClosingCost")]
        [RegularExpression(@"^[+-]?\$?([0-9]+(,[0-9]{3})*)?(\.([0-9]+)?)?$",
        ErrorMessage = "Please Enter Proper Amount.")]
        public decimal BuyersClosingCost { get; set; }
          [Required(ErrorMessage = "Please Enter Close of Escrow")]
        [Display(Name = "CloseofEscrow")]
        [RegularExpression(@"^\$?[0-9]{1,3}(?:\.[0-9]{2})?$",
        ErrorMessage = "Please Enter Proper Days.")]
        public int CloseofEscrow { get; set; }
        public int OfferGroupId { get; set; }
        public bool Contingent { get; set; }
        public string Comments { get; set; }
       

    }
}