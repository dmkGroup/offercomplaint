﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Offer.Models.Offer
{
    public class NewOfferGroupModel : NewOfferModel
    {
        [Required(ErrorMessage = "Please Enter Buyer First Name")]
        [Display(Name = "First Name")]
        public string BuyerFirstname { get; set; }
        [Required(ErrorMessage = "Please Enter Buyer Last Name")]
        [Display(Name = "First Name")]
        public string BuyerLastname { get; set; }
        [Required(ErrorMessage = "Please Enter Agent First Name")]
        [Display(Name = "First Name")]
        public string AgentFirstname { get; set; }
        [Required(ErrorMessage = "Please Enter Buyer Last Name")]
        [Display(Name = "First Name")]
        public string AgentLastname { get; set; }
        public string Brokrage { get; set; }
        public string AgentPhoneNo { get; set; }
        public Nullable<int> ListingId { get; set; }
        public Nullable<int> BuyerGroupId { get; set; }
        public string OfferType { get; set; }
        public Guid ListingUniqueId { get; set; }
    }
}