﻿
using Offer.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Offer.Models.Offer
{
    public class NewInquiryModel : Inquiry
    {
        public bool Lender { get; set; }
        public bool PreApproval { get; set; }
        public bool SellerBuyHome { get; set; }
        public bool Interior { get; set; }
        public bool Schedule { get; set; }

    }
}