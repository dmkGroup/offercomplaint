﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Offer.Models.Offer
{
    public class OfferGroupSearchModel
    {
        public OfferGroupSearchModel()
        {
            OfferGroupItems = new List<OfferGroupGridItem>();
        }

        public List<OfferGroupGridItem> OfferGroupItems { get; set; }
    }

    public class OfferGroupGridItem
    {
        public int OfferGroupId { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string MlsNumber { get; set; }
        public string SellerFirstName { get; set; }
        public string SellerLastName { get; set; }
        public string Status { get; set; }
        public Nullable<int> ListAmount { get; set; }
        public string BuyerFirstname { get; set; }
        public string BuyerLastname { get; set; }
        public string AgentFirstname { get; set; }
        public string AgentLastname { get; set; }
        public string AgentEmail { get; set; }
        public string AgentPhone { get; set; }
        public Nullable<System.DateTime> OfferDate { get; set; }
        public Nullable<decimal> OfferAmount { get; set; }
        public string LoanType { get; set; }
        public string InternalNote { get; set; }

        public Nullable<System.DateTime> CounterOfferDate { get; set; }
        public Nullable<decimal> CounterOfferAmount { get; set; }
        public string CounterLoanType { get; set; }
        public string CounterInternalNote { get; set; }

        public int? OfferId { get; set; }

        public int? CounterOfferId { get; set; }
    }

    public class OfferSearchModel
    {
        public OfferSearchModel()
        {
            OfferItems = new List<OfferGridItem>();
        }
        public List<OfferGridItem> OfferItems { get; set; }
    }

    public class OfferGridItem
    { 
        //New Fields
        public int? OfferId { get; set; }
        public int BuyerCounter { get; set; }
        public Nullable<System.DateTime> OfferExpiryDate { get; set; }
        public Nullable<decimal> PurchasePrice { get; set; }
        public Nullable<decimal> Deposite { get; set; }
        public Nullable<decimal> ClosingCost { get; set; }
        public string Contingent { get; set; }
        public string Financing { get; set; }
        public Nullable<decimal> LoanAmount { get; set; }
        public int CloseofEscrow { get; set; }
        public string BuyersAgent { get; set; }
        public string loanstatus { get; set; }
        public string offerstatus { get; set; }
        public string Note { get; set; }

    }
}