﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Offer.Models.Seller
{
    public class NewSellerModel
    {
        [Required(ErrorMessage = "Please Enter First Name")]
        [Display(Name = "First Name")]
        public string Firstname { get; set; }
        [Required(ErrorMessage = "Please Enter Last Name")]
        [Display(Name = "Name")]
        public string Lastname { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        //[Required(ErrorMessage = "Please Enter Phone No")]
        [Display(Name = "Phone")]
        [RegularExpression(@"((?:\d){10})($|x(?:\d){1,10}?)$",
        ErrorMessage = "Please Enter Correct Phone No")]
        public string Phone { get; set; }
        //[Required(ErrorMessage = "Please Enter Email Address")]
        [Display(Name = "Email")]
        [RegularExpression(@"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*",
        ErrorMessage = "Please Enter Correct Email Address")]
        public string Email { get; set; }
        public string Note { get; set; }
        public System.DateTime DateCreated { get; set; }
        public System.DateTime DateModified { get; set; }
        public int ListingId { get; set; }
    }
}