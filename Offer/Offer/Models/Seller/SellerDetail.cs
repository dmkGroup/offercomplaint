﻿using Offer.Models.Offer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Offer.Models.Seller
{
    public class SellerDetail
    {
        public int SellerId { get; set; }
        public int ListingId { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Note { get; set; }
        public System.DateTime DateCreated { get; set; }
        public System.DateTime DateModified { get; set; }
    }
}