﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Offer.Models.Offer;


namespace Offer.Models.Buyer
{
    public class SearchBuyerGroupModel
    {
        public List<BuyerGroupItem> BuyerGroupItems { get; set; }
    }
    public class SearchBuyerModel
    {
        public List<BuyerItem> BuyerItems { get; set; }
    }
    public class BuyerGroupItem
    {
        public int BuyerGroupId { get; set; }
        public string BuyerGroupName { get; set; }
        public string AgentFirstname { get; set; }
        public string AgentLastname { get; set; }
        public string AgentEmail { get; set; }
        public string AgentPhone { get; set; }
        public int AgentUserId { get; set; }
        public decimal Target_Price { get; set; }
        public string SearchAreas { get; set; }
        public string BuyerNote { get; set; }
        public System.DateTime DateCreated { get; set; }
        public System.DateTime DateModified { get; set; }
        public int CreatedByUserId { get; set; }
        public int ModifiedByUserId { get; set; }
        public System.Guid UniqueId { get; set; }
        public int OrganizationId { get; set; }
        public List<BuyerItem> BuyersList { get; set; }
        public string Status_LookupCode { get; set; }
        public OfferGroupSearchModel OfferGroups { get; set; }
    }

    public class BuyerItem
    {
        public int BuyerId { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Note { get; set; }
        public System.DateTime DateCreated { get; set; }
        public System.DateTime DateModified { get; set; }
        public int CreatedByUserId { get; set; }
        public int ModifiedByUserId { get; set; }
        public int ListingId { get; set; }
        public int BuyerGroupId { get; set; }



    }
}