﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Offer.Models.Buyer
{
    public class NewBuyerGroupModel 
    {
        public int BuyerGroupId { get; set; }
        public string BuyerGroupName { get; set; }
        public string AgentFirstname { get; set; }
        public string AgentLastname { get; set; }
        public string AgentEmail { get; set; }
        public string AgentPhone { get; set; }
        public int AgentUserId { get; set; }
        public decimal Target_Price { get; set; }
        public string SearchAreas { get; set; }
        public string BuyerNote { get; set; }
        public System.DateTime DateCreated { get; set; }
        public System.DateTime DateModified { get; set; }
        public int CreatedByUserId { get; set; }
        public int ModifiedByUserId { get; set; }
        public System.Guid UniqueId { get; set; }
        public int OrganizationId { get; set; }
        public List<BuyerDetailModel> BuyersList { get; set; }
       
    }
}