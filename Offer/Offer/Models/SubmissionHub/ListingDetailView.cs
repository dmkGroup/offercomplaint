﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Offer.Models.SubmissionHub
{
    public class ListingDetailView
    {
        public string Address { get; set; }

        public string ListAmount { get; set; }

        public string ListDate { get; set; }
        public string ExpiryDate { get; set; }

        public string Instructions { get; set; }

        public string ListAgentName { get; set; }

        public string ListAgentEmail { get; set; }

        public string ListAgentCompany { get; set; }

        public string ListAgentPhone { get; set; }

        public string PropertyDetailsUrl { get; set; }

        public int ListingId { get; set; }

        public string ListingStatus { get; set; }

        public bool? IsAgentOffer { get; set; }

        public string ListingUniqueId { get; set; }

    }
}