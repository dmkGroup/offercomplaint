﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Offer.Startup))]
namespace Offer
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
