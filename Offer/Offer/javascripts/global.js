// Main Listing statusFilter script starts here

$("#statusFilter").change(function() {
    var selectedStatusType = this.options[this.selectedIndex].value;
    if (selectedStatusType == "all"){
        $('.listingWrap').removeClass('hidden');
    }   else{
        $('.listingWrap').addClass('hidden');
        $('.listingWrap[data-event-type="' + selectedStatusType + '"]').removeClass('hidden');
    }
});

// Main Listing statusFilter script ends here



// listingBox Filter script starts here

//var selectedScheme = 'active';
//$('#color-scheme').change(function(){
//    $('.listingBox').removeClass(selectedScheme).addClass($(this).val());
//    selectedScheme = $(this).val();
//});

// listingBox Filter script ends here


// navbar toggle animation starts here

$(document).ready(function () {
    $(".navbar-toggle").on("click", function() {
        $(this).toggleClass("active");
    });
});

// navbar toggle animation ends here

// Change input group addon color on focus starts here



// Change input group addon color on focus ends here


//disable/enable submit button

//disable/enable submit button


// Steps

$(document).ready(function () {
  var navListItems = $('div.setup-panel div a'),
          allWells = $('.setup-content'),
          allNextBtn = $('.nextBtn');
       

  allWells.hide();

  navListItems.click(function (e) {
      e.preventDefault();
      var $target = $($(this).attr('href')),
              $item = $(this);

      if (!$item.hasClass('disabled')) {
          navListItems.removeClass('btn-primary').addClass('btn-default');
          $item.addClass('btn-primary');
          allWells.hide();
          $target.show();
          $target.find('input:eq(0)').focus();
      }
  });

  allNextBtn.click(function(){
      var curStep = $(this).closest(".setup-content"),
          curStepBtn = curStep.attr("id"),
          nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
          curInputs = curStep.find("input[type='text'],input[type='url']"),
          isValid = true;

      $(".form-group").removeClass("has-error");
      for(var i=0; i<curInputs.length; i++){
          if (!curInputs[i].validity.valid){
              isValid = false;
              $(curInputs[i]).closest(".form-group").addClass("has-error");
          }
      }

      if (isValid)
          nextStepWizard.removeAttr('disabled').trigger('click');
  });
 
  $('div.setup-panel div a.btn-primary').trigger('click');
});
