﻿using Offer.Api;
using Offer.Data;
using Offer.Models.Buyer;
using Offer.Models.Offer;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace OfferManagement.Mvc.Controllers
{
    public class BuyerController : Controller
    {

        OfferApiController api = new OfferApiController();
        
        // GET: Buyer
        public ActionResult Index()
        {
            return View();
        }
        public PartialViewResult _DispalyBuyerGroup()
        {
            List<BuyerGroup> groups = api.GetAllBuyerGroup();
            SearchBuyerGroupModel model = new SearchBuyerGroupModel();
            model.BuyerGroupItems = new List<BuyerGroupItem>();
            foreach (BuyerGroup group in groups)
            {
                model.BuyerGroupItems.Add(new BuyerGroupItem()
                {
                    BuyerGroupId = group.BuyerGroupId,
                    BuyerGroupName = group.BuyerGroupName,
                    AgentFirstname = group.AgentFirstname,
                    AgentLastname = group.AgentLastname,
                    AgentEmail = group.AgentEmail,
                    AgentPhone = group.AgentPhone,
                    Target_Price = group.Target_Price,
                    SearchAreas = group.SearchAreas,
                    BuyerNote = group.BuyerNote,
                });

            }

            foreach (BuyerGroupItem item in model.BuyerGroupItems)
            {
                item.BuyersList = new List<BuyerItem>();
                List<Buyer> buyers = api.GetBuyerByGroupId(item.BuyerGroupId);
                foreach (var buyer in buyers)
                {
                    item.BuyersList.Add(new BuyerItem()
                    {
                        Firstname = buyer.Firstname,
                        Lastname = buyer.Lastname,
                        Email = buyer.Email,
                        Phone = buyer.Phone,
                        Address = buyer.Address,
                        City = buyer.City,
                        State = buyer.State,
                        PostalCode = buyer.PostalCode,
                    });
                }
                item.OfferGroups = new OfferGroupSearchModel();
                List<OfferGroupSummary> offerGroups = api.OfferGroupSummarySearch(item.BuyerGroupId);
                foreach (OfferGroupSummary offerGroup in offerGroups)
                {
                    item.OfferGroups.OfferGroupItems.Add(new OfferGroupGridItem()
                    {
                        AgentEmail = offerGroup.AgentEmail,
                        AgentFirstname = offerGroup.AgentFirstname,
                        AgentLastname = offerGroup.AgentLastname,
                        BuyerFirstname = offerGroup.BuyerFirstname,
                        BuyerLastname = offerGroup.BuyerLastname,
                        InternalNote = offerGroup.InternalNote,
                        //LoanType = offerGroup.LoanType != null ? offerGroup.LoanType.Value : "",
                        OfferAmount = offerGroup.OfferAmount,
                        OfferDate = offerGroup.OfferDate,
                        CounterInternalNote = offerGroup.CounterInternalNote,
                        //CounterLoanType = offerGroup.CounterLoanType != null ? offerGroup.CounterLoanType.Value : "",
                        CounterOfferAmount = offerGroup.CounterOfferAmount,
                        CounterOfferDate = offerGroup.CounterOfferDate,
                        CounterOfferId = offerGroup.CounterOfferId,
                        OfferId = offerGroup.OfferId
                    });
                }


            }

            return PartialView(model.BuyerGroupItems);
        }
      
        [HttpGet]
        public ActionResult NewBuyerGroup()
        {
            NewBuyerGroupModel model = new NewBuyerGroupModel();
            BuyerGroup bg = new BuyerGroup();
            bg.BuyerGroupName = "Your Group Name";
            //Get usder detail from session 
            //bg.AgentFirstname = Convert.ToInt32(Session["UserId"]);
            //bg.AgentLastname = CurrentUser.Lastname;           

            api.SaveBuyerGroup(bg);
             return RedirectToAction("Detail", new { id = bg.BuyerGroupId });
            
        }
        public PartialViewResult _EditBuyerGroup(int BuyerGroupId)
        {
            BuyerGroup model = api.GetBuyerGroupDetail(BuyerGroupId);
            BuyerGroupItem bg = new BuyerGroupItem();
            bg.BuyerGroupId = model.BuyerGroupId;
            bg.BuyerGroupName = model.BuyerGroupName;
            bg.AgentFirstname = model.AgentFirstname;
            bg.AgentLastname = model.AgentLastname;
            bg.AgentEmail = model.AgentEmail;
            bg.AgentPhone = model.AgentPhone;
            bg.Target_Price = model.Target_Price;
            bg.SearchAreas = model.SearchAreas;
            bg.BuyerNote = model.BuyerNote;
            return PartialView(bg);
        }
        [HttpPost]
        public ActionResult EditBuyerGroup(NewBuyerGroupModel model)
        {
            BuyerGroup bg = new BuyerGroup();
            bg.BuyerGroupId = model.BuyerGroupId;
            bg.BuyerGroupName = model.BuyerGroupName;
            bg.AgentFirstname = model.AgentFirstname;
            bg.AgentLastname = model.AgentLastname;
            bg.AgentEmail = model.AgentEmail;
            bg.AgentPhone = model.AgentPhone;
            bg.Target_Price = model.Target_Price;
            bg.SearchAreas = model.SearchAreas;
            bg.BuyerNote = model.BuyerNote;
            
            api.EditBuyerGroup(bg);

            return RedirectToAction("Detail", new { id = bg.BuyerGroupId });
        }
   
     public ActionResult Detail(int id)
        {
            BuyerGroup model = api.GetBuyerGroupDetail(id);
            BuyerGroupItem bg = new BuyerGroupItem();
            bg.BuyerGroupId = model.BuyerGroupId;
            bg.BuyerGroupName = model.BuyerGroupName;
            bg.AgentFirstname = model.AgentFirstname;
            bg.AgentLastname = model.AgentLastname;
            bg.AgentEmail = model.AgentEmail;
            bg.AgentPhone = model.AgentPhone;
            bg.Target_Price = model.Target_Price;
            bg.SearchAreas = model.SearchAreas;
            bg.BuyerNote = model.BuyerNote;
            SearchBuyerModel buyerdetail = new SearchBuyerModel();
            buyerdetail.BuyerItems = new List<BuyerItem>();
            List<Buyer> buyers = api.GetBuyerByGroupId(id);
            foreach (var buyer in buyers)
            {
                buyerdetail.BuyerItems.Add(new BuyerItem
                {
                    Firstname = buyer.Firstname,
                    Lastname = buyer.Lastname,
                    Email = buyer.Email,
                    Phone = buyer.Phone,
                    Address = buyer.Address,
                    City = buyer.City,
                    State = buyer.State,
                    PostalCode = buyer.PostalCode,
                });
            }
            OfferGroupSearchModel offerGroupSearchModel = new OfferGroupSearchModel();
            List<OfferGroupSummary> offerGroups =api.OfferGroupSummarySearch(id);
            foreach (OfferGroupSummary offerGroup in offerGroups)
            {
                offerGroupSearchModel.OfferGroupItems.Add(new OfferGroupGridItem()
                {
                    AgentEmail = offerGroup.AgentEmail,
                    AgentFirstname = offerGroup.AgentFirstname,
                    AgentLastname = offerGroup.AgentLastname,
                    BuyerFirstname = offerGroup.BuyerFirstname,
                    BuyerLastname = offerGroup.BuyerLastname,
                    InternalNote = offerGroup.InternalNote,
                    //LoanType = offerGroup.LoanType != null ? offerGroup.LoanType.Value : "",
                    OfferAmount = offerGroup.OfferAmount,
                    OfferDate = offerGroup.OfferDate,
                    CounterInternalNote = offerGroup.CounterInternalNote,
                    //CounterLoanType = offerGroup.CounterLoanType != null ? offerGroup.CounterLoanType.Value : "",
                    CounterOfferAmount = offerGroup.CounterOfferAmount,
                    CounterOfferDate = offerGroup.CounterOfferDate,
                    CounterOfferId = offerGroup.CounterOfferId,
                    OfferId = offerGroup.OfferId
                });
            }

           bg.OfferGroups = offerGroupSearchModel;
            bg.BuyersList = buyerdetail.BuyerItems;
            return View(bg);
        }
     public PartialViewResult NewBuyer(int BuyerGroupId)
     {
         TempData["BuyerGroupId"] = BuyerGroupId;
         return PartialView();
     }
     [HttpPost]
     public ActionResult NewBuyer(NewBuyerModel model)
     {
         Buyer buyer = new Buyer();
         buyer.BuyerGroupId = model.BuyerGroupId;
         buyer.Firstname = model.Firstname;
         buyer.Lastname = model.Lastname;
         buyer.Email = model.Email;
         buyer.Phone = model.Phone;
         buyer.Address = model.Address;
         buyer.City = model.City;
         buyer.State = model.State;
         buyer.PostalCode = model.PostalCode;        
         buyer.DateCreated = DateTime.Now;         
         buyer.DateModified = DateTime.Now;
        
         api.SaveBuyer(buyer);
         return RedirectToAction("Detail", new { id = model.BuyerGroupId });
     }
     public PartialViewResult _SearchResult(string SearchType, string Name)
     {
         List<Buyer> sellers;
       
         if (SearchType == "All")
         {
             if (Name != null)
             {
                 sellers = api.GetBuyerByName(Name);
             }
             else
             {
                 sellers = api.GetAllBuyerList();
             }
         }
         else if (SearchType == "Active")
         {
             if (Name != null)
             {
                 sellers = api.GetAllBuyerList();
             }
             else
             {
                 sellers = api.GetAllBuyerList();
             }
         }
         else
         {
             sellers = api.GetAllBuyerList();

         }

         SearchBuyerModel model = new SearchBuyerModel();
         model.BuyerItems = new List<BuyerItem>();
         foreach (var seller in sellers)
         {
             model.BuyerItems.Add(
                new BuyerItem
                {
                    ListingId = seller.ListingId,
                    Firstname = seller.Firstname,
                    Lastname = seller.Lastname,
                    Email = seller.Email,
                    Phone = seller.Phone,
                    Address = seller.Address,
                    City = seller.City,
                    State = seller.State,
                    PostalCode = seller.PostalCode,

                });

         }
         return PartialView(model.BuyerItems);
     }

     //public PartialViewResult NewBuyer(int ListingID)
     //{
     //    TempData["ListingID"] = ListingID;
     //    return PartialView();
     //}
     //[HttpPost]
     //public ActionResult NewBuyer(NewBuyerModel model)
     //{
     //    Buyer seller = new Buyer();
     //    seller.ListingId = model.ListingId;
     //    seller.Firstname = model.Firstname;
     //    seller.Lastname = model.Lastname;
     //    seller.Email = model.Email;
     //    seller.Phone = model.Phone;
     //    Listing listing = this.ListingService.GetListing(model.ListingId);
     //    seller.Address = listing.Address;
     //    seller.City = listing.City;
     //    seller.State = listing.State;
     //    seller.PostalCode = listing.PostalCode;
     //    seller.CreatedByUserId = CurrentUser.UserId;
     //    seller.DateCreated = DateTime.Now;
     //    seller.ModifiedByUserId = CurrentUser.UserId;
     //    seller.DateModified = DateTime.Now;
     //    BuyerService ss = new BuyerService(CurrentUser);
     //    ss.SaveBuyer(seller);
     //    return RedirectToAction("Detail", new { id = model.ListingId });
     //}
     public PartialViewResult BuyerList(int id)
     {
         BuyerGroup model = api.GetBuyerGroupDetail(id);
         BuyerGroupItem bg = new BuyerGroupItem();
         bg.BuyerGroupId = model.BuyerGroupId;
         bg.BuyerGroupName = model.BuyerGroupName;
         bg.AgentFirstname = model.AgentFirstname;
         bg.AgentLastname = model.AgentLastname;
         bg.AgentEmail = model.AgentEmail;
         bg.AgentPhone = model.AgentPhone;
         bg.Target_Price = model.Target_Price;
         bg.SearchAreas = model.SearchAreas;
         bg.BuyerNote = model.BuyerNote;
         SearchBuyerModel buyerdetail = new SearchBuyerModel();
         buyerdetail.BuyerItems = new List<BuyerItem>();
         List<Buyer> buyers = api.GetBuyerByGroupId(id);
         foreach (var buyer in buyers)
         {
             buyerdetail.BuyerItems.Add(new BuyerItem
             {
                 BuyerId=buyer.BuyerId,
                 BuyerGroupId = Convert.ToInt32(buyer.BuyerGroupId),
                 Firstname = buyer.Firstname,
                 Lastname = buyer.Lastname,
                 Email = buyer.Email,
                 Phone = buyer.Phone,
                 Address = buyer.Address,
                 City = buyer.City,
                 State = buyer.State,
                 PostalCode = buyer.PostalCode,
             });
         }
         return PartialView(buyerdetail.BuyerItems);
     }
     public PartialViewResult EditBuyer(int BuyerId)
     {

         var seller = api.GetBuyerDetail(BuyerId);
         return PartialView(seller);
     }
     [HttpPost]
     public ActionResult EditBuyer(Buyer model)
     {
         Buyer seller = new Buyer();
         seller.BuyerId = model.BuyerId;
         seller.ListingId = model.ListingId;
         seller.Firstname = model.Firstname;
         seller.Lastname = model.Lastname;
         seller.Email = model.Email;
         seller.Phone = model.Phone;
         seller.Address = model.Address;
         seller.City = model.City;
         seller.State = model.State;
         seller.PostalCode = model.PostalCode;
         seller.CreatedByUserId = Convert.ToInt32(Session["UserId"]);
         seller.DateCreated = DateTime.Now;
         seller.ModifiedByUserId = Convert.ToInt32(Session["UserId"]);
         seller.DateModified = DateTime.Now;
        
         api.EditBuyer(seller);
         return RedirectToAction("Detail", new { id = model.BuyerGroupId });
         //return Json(new { Success = "true", Message = "Buyer Edited" }, JsonRequestBehavior.DenyGet);
     }
     [HttpPost]
     public JsonResult DeleteBuyer(int BuyerId)
     {
        
         api.DeleteBuyer(BuyerId);
         return Json(new { Success = "true", Message = "Buyer Deleted" }, JsonRequestBehavior.DenyGet);
     }
     public ActionResult _DisplayOffer(int BuyerGroupId)
     {

         ViewBag.StatusResult = api.GetOfferStatus();
         OfferGroupSearchModel offerGroupSearchModel = new OfferGroupSearchModel();
         List<OfferGroupSummary> offerGroups = api.BuyerGroupOffers(BuyerGroupId);
         OfferSearchModel offerSearchModel = new OfferSearchModel();
         foreach (OfferGroupSummary offerGroup in offerGroups)
         {
             offerGroupSearchModel.OfferGroupItems.Add(new OfferGroupGridItem()
             {
                 OfferGroupId = offerGroup.OfferGroupId,
                 AgentEmail = offerGroup.AgentEmail,
                 AgentFirstname = offerGroup.AgentFirstname,
                 AgentLastname = offerGroup.AgentLastname,
                 BuyerFirstname = offerGroup.BuyerFirstname,
                 BuyerLastname = offerGroup.BuyerLastname,
                 InternalNote = offerGroup.InternalNote,
                 //LoanType = offerGroup.LoanType != null ? offerGroup.LoanType.Value : "",
                 OfferAmount = offerGroup.OfferAmount,
                 OfferDate = offerGroup.OfferDate,
                 CounterInternalNote = offerGroup.CounterInternalNote,
                 //CounterLoanType = offerGroup.CounterLoanType != null ? offerGroup.CounterLoanType.Value : "",
                 CounterOfferAmount = offerGroup.CounterOfferAmount,
                 CounterOfferDate = offerGroup.CounterOfferDate,
                 CounterOfferId = offerGroup.CounterOfferId,
                 OfferId = offerGroup.OfferId,


             });
             List<Offer.Data.Offer> offers = api.GetOfferByGroup(offerGroup.OfferGroupId);
             foreach (Offer.Data.Offer off in offers)
             {
                 offerSearchModel.OfferItems.Add(new OfferGridItem()
                 {
                     OfferId = off.OfferId,
                     CloseofEscrow = Convert.ToInt32(off.CloseofEscrow),
                     OfferExpiryDate = off.ExpirationDate,
                     PurchasePrice = off.OfferAmount,
                     Deposite = off.EarnestDeposit,
                     ClosingCost = off.BuyerClosingCosts,
                     Contingent = off.Contingent != true ? "No" : "Yes",
                     Financing = off.LoanType_LookupCode,
                     LoanAmount = off.LoanAmount,
                     BuyersAgent = offerGroup.AgentFirstname + " " + offerGroup.AgentLastname,
                     loanstatus = offerGroup.LoanType_LookupCode,
                     offerstatus = off.OfferStatus_LookupCode,
                     Note = off.Comments
                 });
             }
         }
         return View(offerSearchModel.OfferItems);
     }
    }
}