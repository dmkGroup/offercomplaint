﻿using DubTech.Framework.Ef;
using DubTech.Framework.Lookups;
using DubTech.Framework.Services;
using DubTech.Infrastructure.Validation;
using DubTech.Infrastructure.Web;
using OfferManagement.Mvc.Core;
using Offer.Models.Register;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OfferManagement.Mvc.Controllers
{
    [AllowAnonymous]
    public class RegisterController : DubController
    {
        private AccountService accountService = null;


        private AccountService AccountService
        {
            get
            {
                if (accountService == null) accountService = new AccountService(CurrentUser);
                return accountService;
            }
        }

        // GET: Register
        public ActionResult Step1()
        {
            Step1View model = new Step1View();
            return View(model);
        }

        [HttpPost]
        public ActionResult Step1(Step1View model)
        {
            Registration newRegistration = new Registration();

            if (ModelState.IsValid)
            {
                newRegistration.CompanyName = model.CompanyName;
                newRegistration.Username = model.Username;
                newRegistration.Password = model.Password;
                newRegistration.Email = model.Email;
                newRegistration.Firstname = model.Firstname;
                newRegistration.Lastname = model.Lastname;
                
                if (AccountService.EmailExists(model.Email)) ModelState.AddModelError("Email", "Email already exists");
            }

            if (this.ModelState.IsValid)
            {
                AccountService.RegisterPartialUser(newRegistration);
                return RedirectToAction("Step2", new { id = newRegistration.RegistrationCode.ToString() });
            }
            else
            {
                return View(model);
            }
        }

        public ActionResult Step2(Guid id)
        {
            BillingView model = new BillingView();
            model.RegistrationCode = id;
            return View(model);
        }

        [HttpPost]
        public ActionResult Step2(BillingView model)
        {
            DubTech.Framework.Ef.User newUser = null;
            try
            {
                newUser = AccountService.ConvertRegistrationToAgent(model.RegistrationCode, model.NameOnCard, model.ExpirationMonth, model.ExpirationYear, CcTypeLookup.SafeFromValue<CcTypeLookup>(model.CardType), model.CreditCardNumber, Request.UserHostAddress);
            }
            catch (ValidationException vEx)
            {
                ModelState.AddModelError(vEx.FieldName ?? "", vEx.Message);
            }

            if(newUser != null)
            {
                Authentication.SignIn(newUser.Email, newUser.UserId.ToString(), newUser.OrganizationId, newUser.Firstname, newUser.Lastname, Request.GetOwinContext(), false);
            }

            if(ModelState.IsValid)
            {
                return RedirectToAction("Confirmation");
            }
            else
            {
                return View(model);
            }
            
        }

        public ActionResult Confirmation()
        {
            return View();
        }
    }
}