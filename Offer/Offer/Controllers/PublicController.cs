﻿using DubTech.Infrastructure.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OfferManagement.Mvc.Controllers
{
    [AllowAnonymous]
    public class PublicController : DubController
    {
        // GET: Public
        
        [ActionName("Contact-Us")]
        public ActionResult ContactUs()
        {
            return View("ContactUs");
        }

        [ActionName("Terms-of-Use")]
        public ViewResult TermsOfUse()
        {
            return View("TermsOfUse");
        }

        [ActionName("Privacy-Policy")]
        public ViewResult PrivacyPolicy()
        {
            return View("PrivacyPolicy");
        }
    }
}