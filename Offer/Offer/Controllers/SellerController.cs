﻿using Offer.Api;
using Offer.Data;
using Offer.Models.Seller;
using Offer.Models.Settings;
using System.Collections.Generic;
using System.Web.Mvc;

namespace OfferManagement.Mvc.Controllers
{
    public class SellerController :  Controller
    {
        
        OfferApiController api = new OfferApiController();
        // GET: Seller
        public ActionResult Index()
        {
            return View();
        }
        //Search Result 7-SEP-16
        public PartialViewResult _SearchResult(string SearchType, string Name)
        {
          List<Seller> sellers;
         

            
            if (SearchType == "All")
            {
                if (Name != null)
                {
                    sellers = api.GetSellerByName(Name);
                }
                else
                {
                    sellers = api.GetAllSellerList();
                }
            }
            else if (SearchType == "Active")
            {
                if (Name != null)
                {
                    sellers = api.GetAllSellerList();
                }
                else
                {
                    sellers = api.GetAllSellerList();
                }
            }           
            else 
            {
                sellers = api.GetAllSellerList();
               
            }
           
          SellerSearchModel model = new SellerSearchModel();
            model.SellersList = new List<SellerGridItem>();
            foreach (var seller in sellers)
            {
                model.SellersList.Add(
                   new SellerGridItem
                   {
                       ListingId = seller.ListingId,
                       Firstname = seller.Firstname,
                       Lastname = seller.Lastname,
                       Email = seller.Email,
                       Phone = seller.Phone,
                       Address = seller.Address,
                       City = seller.City,
                       State = seller.State,
                       PostalCode = seller.PostalCode,

                   });

            }
            return PartialView(model.SellersList);
        }
        //Default List 7-SEP-16
        public PartialViewResult _DisplayAll()
        {


            List<Seller> sellers = api.GetAllSellerList();
            SellerSearchModel model = new SellerSearchModel();
            model.SellersList = new List<SellerGridItem>();
            foreach (Seller seller in sellers)
            {
                model.SellersList.Add(
                    new SellerGridItem
                    {
                        ListingId = seller.ListingId,
                        Firstname = seller.Firstname,
                        Lastname = seller.Lastname,
                        Email = seller.Email,
                        Phone = seller.Phone,
                        Address = seller.Address,
                        City = seller.City,
                        State = seller.State,
                        PostalCode = seller.PostalCode,
                        Note=seller.Note

                    });

            }
            return PartialView(model.SellersList);
        }
    }
}