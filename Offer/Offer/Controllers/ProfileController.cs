﻿using DubTech.Framework.Ef;
using Offer.Models.Account;
using OfferManagement.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using DubTech.Infrastructure.Web;

namespace OfferManagement.Mvc.Controllers
{
    public class ProfileController : DubController
    {
        public ProfileController()
        {
            this.userService = new UserService(CurrentUser);

        }

        private UserService userService { get; set; }
        // GET: Profile
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult MyProfile()
        {
            User user = userService.GetUserDetail(1);
            ProfileData model = new ProfileData();
            model.UserId = user.UserId;
            model.Firstname = user.Firstname;
            model.Lastname = user.Lastname;
            model.Email = user.Email;
            model.PhoneNumber = user.PhoneNumber;
            model.CompanyName = user.CompanyName;
            model.LicenseNumber = user.LicenseNumber;
            model.Enabled = user.Enabled;
            model.Password = user.Password;
            return View(model);
        }
        //public ActionResult EditProfile()
        //{
        //    User user = UserService.GetUserDetail(1);
        //    ProfileData model = new ProfileData();
        //    model.UserId = user.UserId;
        //    model.Firstname = user.Firstname;
        //    model.Lastname = user.Lastname;
        //    model.Email = user.Email;
        //    model.PhoneNumber = user.PhoneNumber;
        //    model.CompanyName = user.CompanyName;
        //    model.LicenseNumber = user.LicenseNumber;
        //    model.Enabled = user.Enabled;
        //    //return View(user);
        //    return View(model);
        //}
        [HttpPost]
        public ActionResult EditProfile(User model, HttpPostedFileBase UserImg)
        {
            if (UserImg != null && UserImg.ContentLength > 0)
            {
                string url = string.Empty;
                string fileName = Guid.NewGuid().ToString();
                string path = string.Empty;
                path = Path.Combine(Server.MapPath("~/Img"), fileName.ToString() + "." + Path.GetExtension(UserImg.FileName).Substring(1));
                UserImg.SaveAs(path);
                url = fileName + "." + Path.GetExtension(UserImg.FileName).Substring(1);
                //model.TableFieldUsedForImage = url;
            }
            else
            {
                User user = userService.GetUserDetail(1);
                //model.TableFieldUsedForImage = user.TableFieldUsedForImage;
            }
            userService.EditUser(model);
            return RedirectToAction("MyProfile");
        }
        public ActionResult ChangePassword()
        {
            return View();
        }
        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordModel model)
        {
            if (ModelState.IsValid)
            {
                User user = userService.GetUserDetail(1);
                if (user != null)
                {
                    if (model.OldPassword == user.Password)
                    {
                        user.Password = model.NewPassword;
                        userService.ChangePassword(user.UserId, user.Password);
                        Session.Clear();
                        Session.Abandon();
                        return RedirectToAction("MyProfile");
                    }
                    else
                    {
                        Response.Write(@"<script language='javascript'>alert('Password is Incorrect.');</script>");
                        return View();
                    }
                }
                else
                {
                    Response.Write(@"<script language='javascript'>alert('User not Exists.');</script>");
                    return View();
                }
            }
            else
            {
                return View();
            }
        }
    }
}