﻿using Offer.Api;
using Offer.Models.Account;
using System;
using System.Web.Mvc;

namespace OfferManagement.Mvc.Controllers
{
    public class SettingsController : Controller
    {
        OfferApiController api = new OfferApiController();
        // GET: Settings
        public ActionResult Index()
        {
            return View();
        }
        public PartialViewResult _Billing()
        {
            return PartialView();
        }
        public PartialViewResult _ListingDefaults()
        {
            return PartialView();
        }
        public PartialViewResult _ManageUsers()
        {
            return PartialView();
        }
        public PartialViewResult _Profile()
        {
           
            var user = api.GetUserDetail(Convert.ToInt32(Session["UserId"]));
            ProfileData pfdata = new ProfileData();
            pfdata.Firstname = user.Firstname;
            pfdata.Lastname = user.Lastname;
            pfdata.Email = user.Email;
            pfdata.PhoneNumber = user.PhoneNumber;

            return PartialView(pfdata);
        }
        public PartialViewResult ChangePassword()
        {
            return PartialView();
        }
        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordModel model)
        {
            if (ModelState.IsValid)
            {
               
                var user = api.GetUserDetail(Convert.ToInt32(Session["UserId"]));
                if (user.Password == model.OldPassword)
                {
                    api.ChangePassword(Convert.ToInt32(Session["UserId"]), model.NewPassword);
                    return Json((new { Success = "True", Message = "Password changed sucessfully." }), JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json((new { Success = "false", Message = "Please fill required feilds." }), JsonRequestBehavior.AllowGet);

                }


            }
            else
            {
                return View();
            }


        }

    }
}