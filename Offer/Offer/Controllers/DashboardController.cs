﻿using DubTech.Infrastructure.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace OfferManagement.Mvc.Controllers
{
    public class DashboardController : DubController
    {
        // GET: Dashboard
        public ActionResult Index()
        {
            return View();
        }
    }
}