﻿using Offer.Api;

using Offer.Communication;
using Offer.Data;
using Offer.Logic;
using Offer.Models.Listing;
using Offer.Models.Offer;
using Offer.Models.Seller;
using Offer.Models.Settings;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
namespace OfferManagement.Mvc.Controllers
{  
    public class ListingController : Controller
    {
        AttachmentService attchser = new AttachmentService();
        OfferApiController api = new OfferApiController();
        #region ListingDetail
        // GET: Listing
        public ActionResult Search()
        {
            ViewBag.StatusResult = api.GetListingStatus();
            List<Listing> listings = api.SearchListings();
            SearchModel model = new SearchModel();
            model.ListingItems = new List<ListingGridItem>();
            foreach (Listing listing in listings)
            {
                model.ListingItems.Add(new ListingGridItem
                {
                    Address = listing.Address,
                    ListingId = listing.ListingId,
                    ListAmount = 450000
                });
            }
            return View(model);
        }

        [HttpPost]
        public JsonResult AutoComplete(string prefix)
        {
            DatabaseRepository<Listing> dblisting = new DatabaseRepository<Listing>();
            var listing = (from a in dblisting.Table
                           where a.Address.Contains(prefix) || a.State.Contains(prefix) || a.City.Contains(prefix)
                           select new
                           {
                               label = a.Address + " " + a.City + " " + a.State,
                               val = a.ListingId
                           }).ToList();

            return Json(listing);
        }
        //Search Result 7-SEP-16
        public PartialViewResult _SearchResult(string SearchType, string Address, string Status)
        {
            ViewBag.StatusResult = api.GetListingStatus();
            List<Listing> listings;
            if (SearchType == "Address")
            {
                listings = api.GetListingByAddress(Address);
            }
            else if (SearchType == "Status")
            {
                if (Status == "all")
                {
                    listings = api.SearchListings();
                }
                else
                {
                    List<Listing> ls = new List<Listing>();
                    string[] ListStatus = Status.Split(',');
                    foreach (var item in ListStatus)
                    {
                        var result = api.GetListingByStatus(item);
                        foreach (var data in result)
                        {
                            ls.Add(new Listing
                            {
                                Address = data.Address,
                                ListingId = data.ListingId,
                                ListAmount = Convert.ToInt32(data.ListAmount),
                                ListDate = data.ListDate,
                                City = data.City,
                                PostalCode = data.PostalCode,
                                State = data.State,
                                Status_LookupCode = data.Status_LookupCode,
                            });
                        }
                    }
                    listings = ls;
                    //listings = this.ListingService.ListingSearchByStatus(Status);
                }
            }
            else
            {
                listings = api.SearchListings();
            }
            SearchModel model = new SearchModel();
            model.ListingItems = new List<ListingGridItem>();
            foreach (Listing listing in listings)
            {
                OfferGroupSearchModel offerGroupSearchModel = new OfferGroupSearchModel();
                List<OfferGroupSummary> offerGroups = api.OfferGroupSummarySearch(listing.ListingId);
                foreach (OfferGroupSummary offerGroup in offerGroups)
                {
                    offerGroupSearchModel.OfferGroupItems.Add(new OfferGroupGridItem()
                    {
                        AgentEmail = offerGroup.AgentEmail,
                        AgentFirstname = offerGroup.AgentFirstname,
                        AgentLastname = offerGroup.AgentLastname,
                        BuyerFirstname = offerGroup.BuyerFirstname,
                        BuyerLastname = offerGroup.BuyerLastname,
                        InternalNote = offerGroup.InternalNote,
                        //LoanType = offerGroup.LoanType != null ? offerGroup.LoanType.Value : "",
                        OfferAmount = offerGroup.OfferAmount,
                        OfferDate = offerGroup.OfferDate,
                        CounterInternalNote = offerGroup.CounterInternalNote,
                        // CounterLoanType = offerGroup.CounterLoanType != null ? offerGroup.CounterLoanType.Value : "",
                        CounterOfferAmount = offerGroup.CounterOfferAmount,
                        CounterOfferDate = offerGroup.CounterOfferDate,
                        CounterOfferId = offerGroup.CounterOfferId,
                        OfferId = offerGroup.OfferId
                    });
                }
                model.ListingItems.Add(new ListingGridItem
                {
                    Address = listing.Address,
                    ListingId = listing.ListingId,
                    ListAmount = Convert.ToInt32(listing.ListAmount),
                    ListDate = listing.ListDate,
                    City = listing.City,
                    PostalCode = listing.PostalCode,
                    State = listing.State,
                    OfferGroups = offerGroupSearchModel,
                    Status_LookupCode = listing.Status_LookupCode,
                });
            }
            return PartialView(model.ListingItems);
        }
        //Default List 7-SEP-16
        public PartialViewResult _DisplayAll()
        {
            ViewBag.StatusResult = api.GetListingStatus();
            List<Listing> listings = api.SearchListings();
            SearchModel model = new SearchModel();
            model.ListingItems = new List<ListingGridItem>();
            foreach (Listing listing in listings)
            {
                OfferGroupSearchModel offerGroupSearchModel = new OfferGroupSearchModel();
                List<OfferGroupSummary> offerGroups = api.OfferGroupSummarySearch(listing.ListingId);
                foreach (OfferGroupSummary offerGroup in offerGroups)
                {
                    offerGroupSearchModel.OfferGroupItems.Add(new OfferGroupGridItem()
                    {
                        AgentEmail = offerGroup.AgentEmail,
                        AgentFirstname = offerGroup.AgentFirstname,
                        AgentLastname = offerGroup.AgentLastname,
                        BuyerFirstname = offerGroup.BuyerFirstname,
                        BuyerLastname = offerGroup.BuyerLastname,
                        InternalNote = offerGroup.InternalNote,
                        // LoanType = offerGroup.LoanType != null ? offerGroup.LoanType.Value : "",
                        OfferAmount = offerGroup.OfferAmount,
                        OfferDate = offerGroup.OfferDate,
                        CounterInternalNote = offerGroup.CounterInternalNote,
                        // CounterLoanType = offerGroup.CounterLoanType != null ? offerGroup.CounterLoanType.Value : "",
                        CounterOfferAmount = offerGroup.CounterOfferAmount,
                        CounterOfferDate = offerGroup.CounterOfferDate,
                        CounterOfferId = offerGroup.CounterOfferId,
                        OfferId = offerGroup.OfferId
                    });
                }

                var attchments = api.GetListingAttachment(listing.ListingId);
                byte[] tempfile;
                if (attchments != null)
                {

                    tempfile = attchser.GetTemporaryFile(attchments.LocalFilename);
                }
                else
                { //Demo Image

                    tempfile = attchser.GetTemporaryFile("49bfb058-801b-4a30-9c6c-0359c8dcc23f");
                }

                model.ListingItems.Add(new ListingGridItem
                {
                    Address = listing.Address,
                    ListingId = listing.ListingId,
                    ListAmount = Convert.ToInt32(listing.ListAmount),
                    ListDate = listing.ListDate,
                    City = listing.City,
                    PostalCode = listing.PostalCode,
                    State = listing.State,
                    OfferGroups = offerGroupSearchModel,
                    Status_LookupCode = listing.Status_LookupCode,
                    PropertyImage = tempfile
                });


            }
            return PartialView(model.ListingItems);
        }
        //Edit Status 8-SEP-16
        public void ChangeStatus(int ListId, string Status)
        {
            api.ChangeListingStatus(ListId, Status);
        }
        public ActionResult Detail(int id)
        {

            ViewBag.state = api.GetAllStates();
            ViewBag.StatusResult = api.GetListingStatus();
            Listing listing = api.GetListingById(id);

            User agent = api.GetUserDetail(Convert.ToInt32(listing.CreatedByUserId));
            DetailModel model = new DetailModel();
            model.ListingId = listing.ListingId;
            model.StreetAddress = listing.Address;
            model.City = listing.City;
            model.State = listing.State;
            model.PostalCode = listing.PostalCode;
            model.AgentEmail = agent.Email;
            model.AgentPhone = agent.PhoneNumber;
            model.ListingAgent = FormatHelper.FormatFullName(agent.Firstname, agent.Lastname);
            model.FormattedAddress = FormatHelper.FormatAddressCityStateZip(listing.Address, listing.City, listing.State, listing.PostalCode, false);
            model.FormattedListDate = listing.ListDate.HasValue ? listing.ListDate.ToString() : "";
            model.FormattedListPrice = listing.ListAmount != null ? String.Format("{0:C}", (int)listing.ListAmount) : "";
            model.ListPrice = Convert.ToInt32(listing.ListAmount);
            //model.PublicUrl = listing.PublicUrl;
            //model.ListingStatus = listing.ListingStatus != null ? listing.ListingStatus.DisplayName.ToUpper() : "";
            model.ListingStatusCode = listing.Status_LookupCode != null ? listing.Status_LookupCode : null;
            model.OfferInstructions = listing.OfferInstructions;
            model.MlsUrl = listing.MlsUrl;
            model.ListDate = listing.ListDate;
            model.UniqueId = listing.UniqueId;
            model.ExpirationDate = listing.ListingExpirationDate;
            OfferGroupSearchModel offerGroupSearchModel = new OfferGroupSearchModel();
            List<OfferGroupSummary> offerGroups = api.OfferGroupSummarySearch(id);
            foreach (OfferGroupSummary offerGroup in offerGroups)
            {
                offerGroupSearchModel.OfferGroupItems.Add(new OfferGroupGridItem()
                {
                    AgentEmail = offerGroup.AgentEmail,
                    AgentFirstname = offerGroup.AgentFirstname,
                    AgentLastname = offerGroup.AgentLastname,
                    BuyerFirstname = offerGroup.BuyerFirstname,
                    BuyerLastname = offerGroup.BuyerLastname,
                    InternalNote = offerGroup.InternalNote,
                    // LoanType = offerGroup.LoanType != null ? offerGroup.LoanType.Value : "",
                    OfferAmount = offerGroup.OfferAmount,
                    OfferDate = offerGroup.OfferDate,
                    CounterInternalNote = offerGroup.CounterInternalNote,
                    //CounterLoanType = offerGroup.CounterLoanType != null ? offerGroup.CounterLoanType.Value : "",
                    CounterOfferAmount = offerGroup.CounterOfferAmount,
                    CounterOfferDate = offerGroup.CounterOfferDate,
                    CounterOfferId = offerGroup.CounterOfferId,
                    OfferId = offerGroup.OfferId,
                });
            }
            model.OfferGroups = offerGroupSearchModel;

            var attchments = api.GetListingAttachment(id);
            model.PropertyImage = attchser.GetTemporaryFile(attchments.LocalFilename);
            return View(model);
        }
        [HttpPost]
        public ActionResult Detail(DetailModel model, List<HttpPostedFileBase> filecabinate)
        {
            Listing listing = api.GetListingById(model.ListingId);
            listing.ListAmount = model.ListPrice;
            listing.Address = model.StreetAddress;
            listing.City = model.City;
            listing.State = model.State;
            listing.PostalCode = model.PostalCode;
            //listing.ListingStatus = model.ListingStatusCode != null ? ListingStatusLookup.FromValue<ListingStatusLookup>(model.ListingStatusCode) : null;
            listing.ListDate = model.ListDate;
            listing.ListAmount = model.ListPrice;
            listing.OfferInstructions = model.OfferInstructions;
            listing.MlsUrl = model.MlsUrl;
            api.EditListing(listing);
            //Add Listing Attachments
            #region Attchment

            Attachment attch = api.GetListingAttachment(listing.ListingId);
            attch.BusinessType_LookupId = "Listing";
            attch.BusinessId = listing.ListingId;
            attch.AttachmentType_LookupId = 2;

            var att_type = api.GetAttachmentTypeDetail(attch.AttachmentType_LookupId);
            List<UploadedFileResult> uploadedFileResults = new List<UploadedFileResult>();
            bool haserror = false;
            string message = "An unknown error has occurred, please try again.";
            if (filecabinate != null || filecabinate.Count() > 0)
            {
                foreach (HttpPostedFileBase postedFileKey in filecabinate)
                {
                    HttpPostedFileBase postedFile = postedFileKey;
                    if (postedFile != null)
                    {
                        string FileName = Path.GetFileName(postedFile.FileName);
                        string extension = Path.GetExtension(postedFile.FileName).ToLower();

                        if (!att_type.Extensions.Contains(extension.TrimStart('.')))
                        {
                            message = "FileTypeNotSupported";
                            haserror = true;

                        }

                        if ((att_type.MaxSizeMegaBytes * 131072) <= postedFile.ContentLength)
                        {
                            message = "FileSizeToBig";
                            haserror = true;

                        }
                        if (haserror == false)
                        {
                            Byte[] fileBytes = new Byte[postedFile.ContentLength];
                            attch.FileBytes = postedFile.InputStream.Read(fileBytes, 0, postedFile.ContentLength);
                            string tempFilename =attchser.SaveTemporaryFile(fileBytes);
                            uploadedFileResults.Add(new UploadedFileResult() { Filename = tempFilename });
                            message = String.Format("Sucessfully uploaded {0} file(s)", Request.Files.Count);
                            attch.OriginalFilename = postedFileKey.FileName;
                            attch.LocalFilename = tempFilename;
                            attch.DateCreated = DateTime.Now;
                            attch.DateModified = DateTime.Now;
                            attch.CreatedByUserId = Convert.ToInt32(Session["UserId"]) == 0 ? 1 : Convert.ToInt32(Session["UserId"]);
                            attch.ModifiedByUserId = Convert.ToInt32(Session["UserId"]) == 0 ? 1 : Convert.ToInt32(Session["UserId"]);
                            attch.IsDeleted = false;
                            api.EditAttachment(attch);

                        }
                    }



                }
            }
            #endregion

            return Detail(model.ListingId);
        }
        [HttpGet]
        public ViewResult NewListing()
        {

            ViewBag.state = api.GetAllStates();
            return View();
        }
        private static Random random = new Random();
        public static string RandomString(int length)
        {
            length = 7;
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
        [HttpPost]
        public ActionResult NewListing(NewListingModel model, List<HttpPostedFileBase> filecabinate)
        {
            try
            {
                //Add Property Information
                #region Listing
                Listing newListing = new Listing();

                newListing.SellerFirstName = model.SellerFirstname;
                newListing.SellerLastName = model.SellerLastname;
                newListing.ListAmount = model.ListAmount;
                newListing.ListDate = DateTime.Now;
                newListing.MlsNumber = "Mls";
                newListing.ListingExpirationDate = DateTime.Now;
                newListing.Address = model.Address;
                newListing.City = model.City;
                newListing.State = model.State;
                newListing.PostalCode = model.PostalCode;
                newListing.Status_LookupCode = "listed";
                newListing.OfferInstructions = model.OfferInstruction;
                newListing.MlsUrl = "test instructions";
                newListing.DetailUrl = "test instructions";
                newListing.IsDeleted = false;
                newListing.UniqueId = RandomString(7);
                newListing.OrganizationId = 1;
                newListing.CreatedByUserId = Convert.ToInt32(Session["UserId"]);
                api.SaveListing(newListing);
                #endregion
                //Add Seller Information
                #region Seller
                Seller newseller = new Seller();
                newseller.ListingId = newListing.ListingId;
                newseller.Firstname = model.SellerFirstname;
                newseller.Lastname = model.SellerLastname;
                newseller.Phone = Regex.Replace(model.SellerPhone, @"[^\w\d]", "");
                newseller.Email = model.SellerEmail;
                newseller.Address = model.SellerAddress;
                newseller.City = model.SellerCity;
                newseller.State = model.SellerState;
                newseller.PostalCode = model.PostalCode;
                newseller.CreatedByUserId = Convert.ToInt32(Session["UserId"]);
                newseller.DateCreated = DateTime.Now;
                newseller.ModifiedByUserId = Convert.ToInt32(Session["UserId"]);
                newseller.DateModified = DateTime.Now;

                api.SaveSeller(newseller);
                #endregion
                //Add Listing Attachments
                #region Attchment
                Attachment attch = new Attachment();
                attch.BusinessType_LookupId = "Listing";
                attch.BusinessId = newListing.ListingId;
                attch.AttachmentType_LookupId = 2;

                var att_type = api.GetAttachmentTypeDetail(attch.AttachmentType_LookupId);
                List<UploadedFileResult> uploadedFileResults = new List<UploadedFileResult>();
                bool haserror = false;
                string message = "An unknown error has occurred, please try again.";
                if (filecabinate != null && filecabinate.Count > 0)
                {
                    foreach (HttpPostedFileBase postedFileKey in filecabinate)
                    {
                        HttpPostedFileBase postedFile = postedFileKey;

                        string FileName = Path.GetFileName(postedFile.FileName);
                        string extension = Path.GetExtension(postedFile.FileName).ToLower();

                        if (!att_type.Extensions.Contains(extension.TrimStart('.')))
                        {
                            message = "FileTypeNotSupported";
                            haserror = true;

                        }

                        if ((att_type.MaxSizeMegaBytes * 131072) <= postedFile.ContentLength)
                        {
                            message = "FileSizeToBig";
                            haserror = true;

                        }
                        if (haserror == false)
                        {
                            Byte[] fileBytes = new Byte[postedFile.ContentLength];
                            attch.FileBytes = postedFile.InputStream.Read(fileBytes, 0, postedFile.ContentLength);
                            string tempFilename =attchser.SaveTemporaryFile(fileBytes);
                            uploadedFileResults.Add(new UploadedFileResult() { Filename = tempFilename });
                            message = String.Format("Sucessfully uploaded {0} file(s)", Request.Files.Count);
                            attch.OriginalFilename = postedFileKey.FileName;
                            attch.LocalFilename = tempFilename;
                            attch.DateCreated = DateTime.Now;
                            attch.DateModified = DateTime.Now;
                            attch.CreatedByUserId = Convert.ToInt32(Session["UserId"]);
                            attch.ModifiedByUserId = Convert.ToInt32(Session["UserId"]);
                            attch.IsDeleted = false;

                            api.SaveAttachment(attch);

                        }

                    }
                }
                #endregion

            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
            return RedirectToAction("Search");
            //return Json(newListing);
        }
        public JsonResult GetSellerData()
        {

            var data = api.GetSellerDetail(Convert.ToInt32(Session["UserId"]));
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #region Offer
        public ActionResult _DisplayOffer(int ListingId)
        {
            TempData["Offer"] = null;

            ViewBag.StatusResult = api.GetOfferStatus();
            OfferGroupSearchModel offerGroupSearchModel = new OfferGroupSearchModel();
            List<OfferGroupSummary> offerGroups = api.OfferGroupSummarySearch(ListingId);
            OfferSearchModel offerSearchModel = new OfferSearchModel();
            foreach (OfferGroupSummary offerGroup in offerGroups)
            {
                offerGroupSearchModel.OfferGroupItems.Add(new OfferGroupGridItem()
                {
                    OfferGroupId = offerGroup.OfferGroupId,
                    AgentEmail = offerGroup.AgentEmail,
                    AgentFirstname = offerGroup.AgentFirstname,
                    AgentLastname = offerGroup.AgentLastname,
                    BuyerFirstname = offerGroup.BuyerFirstname,
                    BuyerLastname = offerGroup.BuyerLastname,
                    InternalNote = offerGroup.InternalNote,
                    // LoanType = offerGroup.LoanType != null ? offerGroup.LoanType.Value : "",
                    OfferAmount = offerGroup.OfferAmount,
                    OfferDate = offerGroup.OfferDate,
                    CounterInternalNote = offerGroup.CounterInternalNote,
                    // CounterLoanType = offerGroup.CounterLoanType != null ? offerGroup.CounterLoanType.Value : "",
                    CounterOfferAmount = offerGroup.CounterOfferAmount,
                    CounterOfferDate = offerGroup.CounterOfferDate,
                    CounterOfferId = offerGroup.CounterOfferId,
                    OfferId = offerGroup.OfferId,


                });
                List<Offer.Data.Offer> offers = api.GetOfferByGroup(offerGroup.OfferGroupId);
                foreach (Offer.Data.Offer off in offers)
                {
                    offerSearchModel.OfferItems.Add(new OfferGridItem()
                    {
                        OfferId = off.OfferId,
                        CloseofEscrow = Convert.ToInt32(off.CloseofEscrow),
                        OfferExpiryDate = off.ExpirationDate,
                        PurchasePrice = off.OfferAmount,
                        Deposite = off.EarnestDeposit,
                        ClosingCost = off.BuyerClosingCosts,
                        Contingent = off.Contingent != true ? "No" : "Yes",
                        Financing = off.LoanType_LookupCode,
                        LoanAmount = off.LoanAmount,
                        BuyersAgent = offerGroup.AgentFirstname + " " + offerGroup.AgentLastname,
                        loanstatus = offerGroup.LoanType_LookupCode,
                        offerstatus = off.OfferStatus_LookupCode,
                        Note = off.Comments
                    });
                }
                TempData["ListId"] = ListingId;
                TempData["Offer"] = offerSearchModel.OfferItems;
            }
            return View(offerSearchModel.OfferItems);
        }
        public JsonResult ChangeOfferStatus(int OfferId, string Status)
        {

            List<OfferGridItem> Offers = TempData["Offer"] as List<OfferGridItem>;
            string message = "Message";

            if (Status == "accepted")
            {
                foreach (var item in Offers)
                {
                    if (item.offerstatus == "accepted")
                    {

                        message = "Offer Can be Accepted only Once.";
                        break;

                    }
                    else
                    {
                        message = "Offer has been " + Status;
                        api.ChangeOfferStatus(OfferId, Status);
                    }
                }
            }
            else
            {
                message = "Offer has been " + Status;
                api.ChangeOfferStatus(OfferId, Status);
            }

            return Json(new { Message = message, ListId = TempData["ListId"] }, JsonRequestBehavior.AllowGet);
        }
        public PartialViewResult _OfferDetail(int OfferId)
        {
            OfferGridItem OfferItems = new OfferGridItem();

            Offer.Data.Offer offer = api.GetOfferDetail(OfferId);


            ViewBag.StatusResult = api.GetOfferStatus();
            OfferItems.OfferId = offer.OfferId;
            OfferItems.CloseofEscrow = Convert.ToInt32(offer.CloseofEscrow);
            OfferItems.OfferExpiryDate = offer.ExpirationDate;
            OfferItems.PurchasePrice = offer.OfferAmount;
            OfferItems.Deposite = offer.EarnestDeposit;
            OfferItems.ClosingCost = offer.BuyerClosingCosts;
            OfferItems.Contingent = offer.Contingent != true ? "No" : "Yes";
            OfferItems.Financing = offer.LoanType_LookupCode;
            OfferItems.LoanAmount = offer.LoanAmount;
            OfferItems.Note = offer.Comments;
            //BuyersAgent = offerGroup.AgentFirstname + " " + offerGroup.AgentLastname,
            //loanstatus = offerGroup.LoanType_LookupCode,
            OfferItems.offerstatus = offer.OfferStatus_LookupCode;
            return PartialView(OfferItems);
        }
        public PartialViewResult _CompareOffers(string data)
        {
            List<OfferGridItem> offers = new List<OfferGridItem>();
            OfferSearchModel offerSearchModel = new OfferSearchModel();
            string[] ArrayOffer = data.Split(',');
            foreach (var item in ArrayOffer)
            {

                OfferGridItem OfferItems = new OfferGridItem();
                Offer.Data.Offer off = api.GetOfferDetail(Convert.ToInt32(item));
                offerSearchModel.OfferItems.Add(new OfferGridItem()
                  {
                      OfferId = off.OfferId,
                      CloseofEscrow = Convert.ToInt32(off.CloseofEscrow),
                      OfferExpiryDate = off.ExpirationDate,
                      PurchasePrice = off.OfferAmount,
                      Deposite = off.EarnestDeposit,
                      ClosingCost = off.BuyerClosingCosts,
                      Contingent = off.Comments,
                      Financing = "FHA",
                      LoanAmount = off.LoanAmount,
                      //BuyersAgent = offerGroup.AgentFirstname + " " + offerGroup.AgentLastname,
                      //loanstatus = offerGroup.LoanType_LookupCode,
                      offerstatus = off.OfferStatus_LookupCode,
                      Note = off.InternalNote
                  });

                offers = offerSearchModel.OfferItems;
            }

            return PartialView(offers);
        }
        public void _EditNote(int OfferId, string Note)
        {

            Offer.Data.Offer off = api.GetOfferDetail(OfferId);
            off.InternalNote = Note;
            api.EditOfferNote(off);
        }
        #endregion
        #region Seller
        public PartialViewResult NewSeller(int ListingID)
        {
            TempData["ListingID"] = ListingID;
            return PartialView();
        }
        [HttpPost]
        public ActionResult NewSeller(NewSellerModel model, HttpPostedFileBase sellerpic)
        {
            if (model.Email == null || model.Phone == null)
            {
                ModelState.Remove("Email");
            }
            if (model.Phone == null)
            {
                ModelState.Remove("Phone");
            }
            if (ModelState.IsValid)
            {
                Seller seller = new Seller();
                seller.ListingId = model.ListingId;
                seller.Firstname = model.Firstname;
                seller.Lastname = model.Lastname;
                seller.Email = model.Email;
                seller.Phone = model.Phone;
                Listing listing = api.GetListingById(model.ListingId);
                seller.Address = listing.Address;
                seller.City = listing.City;
                seller.State = listing.State;
                seller.PostalCode = listing.PostalCode;
                seller.CreatedByUserId = Convert.ToInt32(Session["UserId"]);
                seller.DateCreated = DateTime.Now;
                seller.ModifiedByUserId = Convert.ToInt32(Session["UserId"]);
                seller.DateModified = DateTime.Now;
               
                api.SaveSeller(seller);

                Attachment attch = new Attachment();
                attch.BusinessType_LookupId = "Profile";
                attch.BusinessId = seller.SellerId;
                attch.AttachmentType_LookupId = 3;//Profile 
               
                var att_type = api.GetAttachmentTypeDetail(attch.AttachmentType_LookupId);
                List<UploadedFileResult> uploadedFileResults = new List<UploadedFileResult>();
                bool haserror = false;
                string message = "An unknown error has occurred, please try again.";
                if (sellerpic != null)
                {

                    HttpPostedFileBase postedFile = sellerpic;

                    string FileName = Path.GetFileName(postedFile.FileName);
                    string extension = Path.GetExtension(postedFile.FileName).ToLower();

                    if (!att_type.Extensions.Contains(extension.TrimStart('.')))
                    {
                        message = "FileTypeNotSupported";
                        haserror = true;

                    }

                    if ((att_type.MaxSizeMegaBytes * 131072) <= postedFile.ContentLength)
                    {
                        message = "FileSizeToBig";
                        haserror = true;

                    }
                    if (haserror == false)
                    {
                        Byte[] fileBytes = new Byte[postedFile.ContentLength];
                        attch.FileBytes = postedFile.InputStream.Read(fileBytes, 0, postedFile.ContentLength);
                        string tempFilename =attchser.SaveTemporaryFile(fileBytes);
                        uploadedFileResults.Add(new UploadedFileResult() { Filename = tempFilename });
                        message = String.Format("Sucessfully uploaded {0} file(s)", Request.Files.Count);
                        attch.OriginalFilename = postedFile.FileName;
                        attch.LocalFilename = tempFilename;
                        attch.DateCreated = DateTime.Now;
                        attch.DateModified = DateTime.Now;
                        attch.CreatedByUserId = Convert.ToInt32(Session["UserId"]);;
                        attch.ModifiedByUserId = Convert.ToInt32(Session["UserId"]);;
                        attch.IsDeleted = false;                        
                        api.SaveAttachment(attch);



                    }
                }

                return RedirectToAction("Detail", new { id = model.ListingId });
            }
            else
            {
                return View();
            }
        }
        public PartialViewResult SellersList(int ListingID)
        {

            List<Seller> sellers = api.GetSellerByListing(ListingID);
            SellerSearchModel model = new SellerSearchModel();
            model.SellersList = new List<SellerGridItem>();



            foreach (Seller seller in sellers)
            {


                var attchments = api.GetProfileAttachment(seller.SellerId);
                byte[] tempfile;
                if (attchments != null)
                {

                    tempfile = attchser.GetTemporaryFile(attchments.LocalFilename);
                }
                else
                { //Demo Image

                    tempfile = attchser.GetTemporaryFile("49bfb058-801b-4a30-9c6c-0359c8dcc23f");
                }
                model.SellersList.Add(
                    new SellerGridItem
                    {
                        SellerId = seller.SellerId,
                        ListingId = seller.ListingId,
                        Firstname = seller.Firstname,
                        Lastname = seller.Lastname,
                        Email = seller.Email,
                        Phone = seller.Phone,
                        Address = seller.Address,
                        City = seller.City,
                        State = seller.State,
                        PostalCode = seller.PostalCode,
                        SellerImage = tempfile
                    });
            }
            return PartialView(model.SellersList);
        }
        public PartialViewResult EditSeller(int SellerId)
        {

            var seller = api.GetSellerDetail(SellerId);

            var attchments = api.GetProfileAttachment(seller.SellerId);
            byte[] tempfile;
            if (attchments != null)
            {

                tempfile = attchser.GetTemporaryFile(attchments.LocalFilename);
            }
            else
            { //Demo Image

                tempfile = attchser.GetTemporaryFile("49bfb058-801b-4a30-9c6c-0359c8dcc23f");
            }
            ViewBag.sellerimage = tempfile;
            return PartialView(seller);
        }
        [HttpPost]
        public ActionResult EditSeller(Seller model, HttpPostedFileBase sellerpic)
        {
            Seller seller = new Seller();
            seller.SellerId = model.SellerId;
            seller.ListingId = model.ListingId;
            seller.Firstname = model.Firstname;
            seller.Lastname = model.Lastname;
            seller.Email = model.Email;
            seller.Phone = model.Phone;
            seller.Address = model.Address;
            seller.City = model.City;
            seller.State = model.State;
            seller.PostalCode = model.PostalCode;
            seller.CreatedByUserId = Convert.ToInt32(Session["UserId"]);
            seller.DateCreated = DateTime.Now;
            seller.ModifiedByUserId = Convert.ToInt32(Session["UserId"]);
            seller.DateModified = DateTime.Now;

            Attachment attch = new Attachment();
            attch.BusinessType_LookupId = "Profile";
            attch.BusinessId = seller.SellerId;
            attch.AttachmentType_LookupId = 3;//Profile 

            var att_type = api.GetAttachmentTypeDetail(attch.AttachmentType_LookupId);
            List<UploadedFileResult> uploadedFileResults = new List<UploadedFileResult>();
            bool haserror = false;
            string message = "An unknown error has occurred, please try again.";
            if (sellerpic != null)
            {

                HttpPostedFileBase postedFile = sellerpic;

                string FileName = Path.GetFileName(postedFile.FileName);
                string extension = Path.GetExtension(postedFile.FileName).ToLower();

                if (!att_type.Extensions.Contains(extension.TrimStart('.')))
                {
                    message = "FileTypeNotSupported";
                    haserror = true;

                }

                if ((att_type.MaxSizeMegaBytes * 131072) <= postedFile.ContentLength)
                {
                    message = "FileSizeToBig";
                    haserror = true;

                }
                if (haserror == false)
                {
                    Byte[] fileBytes = new Byte[postedFile.ContentLength];
                    attch.FileBytes = postedFile.InputStream.Read(fileBytes, 0, postedFile.ContentLength);
                    string tempFilename =attchser.SaveTemporaryFile(fileBytes);
                    uploadedFileResults.Add(new UploadedFileResult() { Filename = tempFilename });
                    message = String.Format("Sucessfully uploaded {0} file(s)", Request.Files.Count);
                    attch.OriginalFilename = postedFile.FileName;
                    attch.LocalFilename = tempFilename;
                    attch.DateCreated = DateTime.Now;
                    attch.DateModified = DateTime.Now;
                    attch.CreatedByUserId = Convert.ToInt32(Session["UserId"]);
                    attch.ModifiedByUserId = Convert.ToInt32(Session["UserId"]);
                    attch.IsDeleted = false;

                    api.SaveAttachment(attch);

                }
            }


            api.EditSeller(seller);
            return RedirectToAction("Detail", new { id = model.ListingId });
            //return Json(new { Success = "true", Message = "Seller Edited" }, JsonRequestBehavior.DenyGet);
        }
        [HttpPost]
        public JsonResult DeleteSeller(int SellerId)
        {

            api.DeleteSeller(SellerId);
            return Json(new { Success = "true", Message = "Seller Deleted" }, JsonRequestBehavior.DenyGet);
        }
        #endregion
        #region File Cabinate
        private class UploadedFileResult
        {
            public string Filename { get; set; }
        }
        public PartialViewResult _FileCabinate(int ListingId)
        {
            ViewBag.ListingId = ListingId;
            return PartialView();
        }
        [HttpPost]
        public JsonResult FileCabinate(List<HttpPostedFileBase> filecabinate, int ListingId)
        {
            Attachment attch = new Attachment();
            attch.BusinessType_LookupId = "Listing";
            attch.BusinessId = ListingId;
            attch.AttachmentType_LookupId = 2;//Property 

            var att_type = api.GetAttachmentTypeDetail(attch.AttachmentType_LookupId);
            List<UploadedFileResult> uploadedFileResults = new List<UploadedFileResult>();
            bool haserror = false;
            string message = "An unknown error has occurred, please try again.";
            if (filecabinate != null && filecabinate.Count > 0)
            {
                foreach (HttpPostedFileBase postedFileKey in filecabinate)
                {
                    HttpPostedFileBase postedFile = postedFileKey;

                    string FileName = Path.GetFileName(postedFile.FileName);
                    string extension = Path.GetExtension(postedFile.FileName).ToLower();

                    if (!att_type.Extensions.Contains(extension.TrimStart('.')))
                    {
                        message = "FileTypeNotSupported";
                        haserror = true;

                    }

                    if ((att_type.MaxSizeMegaBytes * 131072) <= postedFile.ContentLength)
                    {
                        message = "FileSizeToBig";
                        haserror = true;

                    }
                    if (haserror == false)
                    {
                        Byte[] fileBytes = new Byte[postedFile.ContentLength];
                        attch.FileBytes = postedFile.InputStream.Read(fileBytes, 0, postedFile.ContentLength);
                        string tempFilename = attchser.SaveTemporaryFile(fileBytes);
                        uploadedFileResults.Add(new UploadedFileResult() { Filename = tempFilename });
                        message = String.Format("Sucessfully uploaded {0} file(s)", Request.Files.Count);
                        attch.OriginalFilename = postedFileKey.FileName;
                        attch.LocalFilename = tempFilename;
                        attch.DateCreated = DateTime.Now;
                        attch.DateModified = DateTime.Now;
                        attch.CreatedByUserId = Convert.ToInt32(Session["UserId"]);
                        attch.ModifiedByUserId = Convert.ToInt32(Session["UserId"]);
                        attch.IsDeleted = false;

                        api.SaveAttachment(attch);

                    }

                }
            }
            return Json(new { Success = true, Message = message, Files = uploadedFileResults }, JsonRequestBehavior.DenyGet);




            //foreach (var item in filecabinate)
            //{
            //    if (item != null && item.ContentLength > 0)
            //    {
            //        string url = string.Empty;
            //        string fileName = Guid.NewGuid().ToString();
            //        string path = string.Empty;
            //        path = Path.Combine(Server.MapPath("~/Img"), fileName.ToString() + "." + Path.GetExtension(item.FileName).Substring(1));
            //        item.SaveAs(path);
            //        url = fileName + "." + Path.GetExtension(item.FileName).Substring(1);
            //    }
            //}
            //return Json(new { Success = "true", Message = "File Uploded." }, JsonRequestBehavior.DenyGet);
        }
        #endregion

        public ActionResult SendCounterOffer(int OfferId)
        {
            OfferGridItem OfferItems = new OfferGridItem();

            Offer.Data.Offer offer = api.GetOfferDetail(OfferId);


            ViewBag.StatusResult = api.GetOfferStatus();
            OfferItems.OfferId = offer.OfferId;
            OfferItems.CloseofEscrow = Convert.ToInt32(offer.CloseofEscrow);
            OfferItems.OfferExpiryDate = offer.ExpirationDate;
            OfferItems.PurchasePrice = offer.OfferAmount;
            OfferItems.Deposite = offer.EarnestDeposit;
            OfferItems.ClosingCost = offer.BuyerClosingCosts;
            OfferItems.Contingent = offer.Contingent != true ? "No" : "Yes";
            OfferItems.Financing = offer.LoanType_LookupCode;
            OfferItems.LoanAmount = offer.LoanAmount;
            OfferItems.Note = offer.Comments;
            //BuyersAgent = offerGroup.AgentFirstname + " " + offerGroup.AgentLastname,
            //loanstatus = offerGroup.LoanType_LookupCode,
            OfferItems.offerstatus = offer.OfferStatus_LookupCode;
            return View(OfferItems);
        }
        [HttpPost]
        public ActionResult SendCounterOffer(FormCollection frm)
        { 
            
           
            return View();
        }
        //public JsonResult SendCounterOffer(int OfferId)
        //{

        //    //OfferService os = new OfferService(CurrentUser);
        //    //var offer = os.GetOfferDetail(OfferId);

        //    //var offergroup = os.GetOfferGroupDetail(Convert.ToInt32(offer.OfferGroupId));

        //    //EmailService emailService = new EmailService(CurrentUser);
        //    //OfferLinkTemplate emailTemplate = emailService.GetEmailTemplateDefinition<OfferLinkTemplate>();

        //    //emailTemplate.OfferUrl = String.Format("{0}{1}{2}", WebConfig.RootUrl, "/SubmissionHub/ViewOffer?OfferUniqueNo=", offergroup.UniqueId);

        //    //string body = string.Empty;
        //    //using (StreamReader reader = new StreamReader(Server.MapPath("~/OfferLinkTemplate.html")))
        //    //{
        //    //    body = reader.ReadToEnd();
        //    //}

        //    ///******************************************************************/
        //    //body = body.Replace("{OfferUrl}", emailTemplate.OfferUrl);


        //    //EmailService es = new EmailService(CurrentUser);
        //    //es.SendEmailOffer(body);

        //    //var result = new { Success = "True", Message = "Error Message", OfferGroupId = offer.OfferGroupId };
        //    //return Json(result, JsonRequestBehavior.AllowGet);
        //}
    }
}