﻿using Offer.Api;
using Offer.Data;
using Offer.Models.Offer;
using Offer.Models.Settings;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OfferManagement.Mvc.Controllers
{
    public class OfferController : Controller
    {
        AttachmentService attchser = new AttachmentService();
        OfferApiController api = new OfferApiController();
       

        // GET: Offer
        public ActionResult Search()
        {
            return View();
        }
        [HttpGet]
        public PartialViewResult NewOffer()
        {
            return PartialView("_NewOffer");
        }
        [HttpPost]
        public JsonResult NewOffer(NewOfferModel model)
        {

            Offer.Data.Offer offer = new Offer.Data.Offer();
            offer.BuyerClosingCosts = model.BuyersClosingCost;
            offer.ClosingDate = model.ClosingDate;
            offer.Comments = model.Comments;
            offer.EarnestDeposit = model.DepositAmount;
            offer.ExpirationDate = model.ExpirationDate;
            offer.IsBuyerSubmitted = true; // New Offer Group always assumes first offer is from buyer
            offer.LoanAmount = model.LoanAmount;
            offer.OfferAmount = model.OfferAmount ?? 0; //todo: offer amount required
            offer.OfferDate = model.OfferDate ?? DateTime.Now;

            api.SaveOffer(offer);

            return Json(new { Success = "true", Message = "Offer Added" }, JsonRequestBehavior.DenyGet);
        }

        
        public PartialViewResult NewOfferGroup(int ListingId, string OfferType)
        {

           
            ViewBag.financing = api.GetLoanTypeStatus();
            NewOfferGroupModel model = new NewOfferGroupModel();
            if (OfferType == "ListingOfferGroup")
            {
                model.ListingId = ListingId;
                model.BuyerGroupId = null;
                model.OfferType = "ListingOfferGroup";
            }
            if (OfferType == "BuyerOfferGroup")
            {
                model.BuyerGroupId = ListingId;
                model.ListingId = null;
                model.OfferType = "BuyerOfferGroup";
                
            }
                      return PartialView("_NewOfferGroup");
        }
        [HttpPost]
        public JsonResult NewOfferGroup(NewOfferGroupModel model, List<HttpPostedFileBase> offerfiles)
        {
            OfferGroup offerGroup = new OfferGroup();
            //Distinguish Agent and Buyer Offer
            if (model.OfferType == "ListingOfferGroup")
            {
                offerGroup.ListingId = model.ListingId;
                offerGroup.BuyerGroupId = null;
                offerGroup.OfferGroupType = "ListingOfferGroup";

            }
            if (model.OfferType == "BuyerOfferGroup")
            {
                offerGroup.BuyerGroupId = model.BuyerGroupId;
                offerGroup.ListingId = null;
                offerGroup.OfferGroupType = "BuyerOfferGroup";
            }

            offerGroup.AgentEmail = model.AgentEmail;
            offerGroup.AgentFirstname = model.AgentFirstname;
            offerGroup.AgentLastname = model.AgentLastname;
            offerGroup.AgentPhone = model.AgentPhoneNo;
            offerGroup.BuyerFirstname = model.BuyerFirstname;
            offerGroup.BuyerLastname = model.BuyerLastname;

            Offer.Data.Offer offer = new Offer.Data.Offer();
            offer.BuyerClosingCosts = model.BuyersClosingCost;
            offer.ClosingDate = Convert.ToDateTime(model.ClosingDate);
            offer.Comments = model.Comments;
            offer.EarnestDeposit = model.DepositAmount;
            offer.ExpirationDate = model.ExpirationDate ?? DateTime.Now;
            offer.IsBuyerSubmitted = true; // New Offer Group always assumes first offer is from buyer
            offer.LoanAmount = model.LoanAmount;
            offer.OfferAmount = model.OfferAmount ?? 0; //todo: offer amount required
            offer.OfferDate = model.OfferDate ?? DateTime.Now;
            offer.CloseofEscrow = model.CloseofEscrow;
            offer.LoanType_LookupCode = model.LoanType;
            offer.OfferStatus_LookupCode = "new";
            offer.Contingent = model.Contingent;
            offer.CreatedByUserId = Convert.ToInt32(Session["UserId"]);
            offer.ModifiedByUserId = Convert.ToInt32(Session["UserId"]);
            // Save Offer Group

            api.OfferGroupSave(offerGroup);
            //Save Offer
            offer.OfferGroupId = offerGroup.OfferGroupId;
        
            api.SaveOffer(offer);
            if (offerfiles != null)
            {
                foreach (var item in offerfiles)
                {

                    if (item != null && item.ContentLength > 0)
                    {
                        string url = string.Empty;
                        string fileName = Guid.NewGuid().ToString();
                        string path = string.Empty;
                        path = Path.Combine(Server.MapPath("~/Img"), fileName.ToString() + "." + Path.GetExtension(item.FileName).Substring(1));
                        item.SaveAs(path);
                        url = fileName + "." + Path.GetExtension(item.FileName).Substring(1);
                    }
                }
            }
            var result = new { Success = "True", Message = "Error Message", Listing = model.ListingId };
            return Json(result, JsonRequestBehavior.AllowGet);

        }
        public JsonResult Upload(List<HttpPostedFileBase> UserImg)
        {
            var ret = new string[UserImg.Count()];
            int i = 0;
            foreach (var item in UserImg)
            {

                if (item != null && item.ContentLength > 0)
                {
                    string url = string.Empty;
                    string fileName = Guid.NewGuid().ToString();
                    string path = string.Empty;
                    path = Path.Combine(Server.MapPath("~/Img"), fileName.ToString() + "." + Path.GetExtension(item.FileName).Substring(1));
                    item.SaveAs(path);
                    url = fileName + "." + Path.GetExtension(item.FileName).Substring(1);
                    ret[i] = url;
                    i++;
                }
            }
            return Json(new { Success = "True", Message = "Error Message", Result = ret }, JsonRequestBehavior.DenyGet);
        }

        public ActionResult _DisplayOffer(int BuyerGroupId)
        {

            ViewBag.StatusResult = api.GetOfferStatus();
            OfferGroupSearchModel offerGroupSearchModel = new OfferGroupSearchModel();
            
            List<OfferGroupSummary> offerGroups = api.OfferGroupSummarySearch(BuyerGroupId);
            OfferSearchModel offerSearchModel = new OfferSearchModel();
            foreach (OfferGroupSummary offerGroup in offerGroups)
            {
                offerGroupSearchModel.OfferGroupItems.Add(new OfferGroupGridItem()
                {
                    OfferGroupId = offerGroup.OfferGroupId,
                    AgentEmail = offerGroup.AgentEmail,
                    AgentFirstname = offerGroup.AgentFirstname,
                    AgentLastname = offerGroup.AgentLastname,
                    BuyerFirstname = offerGroup.BuyerFirstname,
                    BuyerLastname = offerGroup.BuyerLastname,
                    InternalNote = offerGroup.InternalNote,
                    //LoanType = offerGroup.LoanType != null ? offerGroup.LoanType.Value : "",
                    OfferAmount = offerGroup.OfferAmount,
                    OfferDate = offerGroup.OfferDate,
                    CounterInternalNote = offerGroup.CounterInternalNote,
                   // CounterLoanType = offerGroup.CounterLoanType != null ? offerGroup.CounterLoanType.Value : "",
                    CounterOfferAmount = offerGroup.CounterOfferAmount,
                    CounterOfferDate = offerGroup.CounterOfferDate,
                    CounterOfferId = offerGroup.CounterOfferId,
                    OfferId = offerGroup.OfferId,


                });
                List<Offer.Data.Offer> offers = api.GetOfferByGroup(offerGroup.OfferGroupId);
                foreach (Offer.Data.Offer off in offers)
                {
                    offerSearchModel.OfferItems.Add(new OfferGridItem()
                    {
                        OfferId = off.OfferId,
                        CloseofEscrow = Convert.ToInt32(off.CloseofEscrow),
                        OfferExpiryDate = off.ExpirationDate,
                        PurchasePrice = off.OfferAmount,
                        Deposite = off.EarnestDeposit,
                        ClosingCost = off.BuyerClosingCosts,
                        Contingent = off.Contingent != true ? "No" : "Yes",
                        Financing = off.LoanType_LookupCode,
                        LoanAmount = off.LoanAmount,
                        BuyersAgent = offerGroup.AgentFirstname + " " + offerGroup.AgentLastname,
                        loanstatus = offerGroup.LoanType_LookupCode,
                        offerstatus = off.OfferStatus_LookupCode,
                        Note = off.Comments
                    });
                }
            }
            return View(offerSearchModel.OfferItems);
        }

        #region File Cabinate
        private class UploadedFileResult
        {
            public string Filename { get; set; }
        }

        public PartialViewResult _FileCabinate(int OfferId)
        {
            ViewBag.OfferId = OfferId;
            //Attachment attch = new Attachment();
            //byte[] tempFilename = DubTech.Framework.Services.AttachmentService.GetTemporaryFile("e3375405-daab-4655-ba36-ae34b661e66d");
            //return PartialView(tempFilename);
            return PartialView();
        }
        [HttpPost]
        public JsonResult FileCabinate(List<HttpPostedFileBase> filecabinate, int OfferId)
        {
            Attachment attch = new Attachment();
            attch.BusinessType_LookupId = "Offer";
            attch.BusinessId = OfferId;
            attch.AttachmentType_LookupId = 4;//Offer
         
          
            var att_type=api.GetAttachmentTypeDetail(attch.AttachmentType_LookupId);
             List<UploadedFileResult> uploadedFileResults = new List<UploadedFileResult>();
             bool haserror = false;
             string message = "An unknown error has occurred, please try again.";
            if (filecabinate != null && filecabinate.Count > 0)
            {
                foreach (HttpPostedFileBase postedFileKey in filecabinate)
                {
                    HttpPostedFileBase postedFile = postedFileKey;

                    string FileName = Path.GetFileName(postedFile.FileName);
                    string extension = Path.GetExtension(postedFile.FileName).ToLower();

                    if (!att_type.Extensions.Contains(extension.TrimStart('.')))
                    {
                        message = "FileTypeNotSupported";
                        haserror = true;
                       
                    }

                    if ((att_type.MaxSizeMegaBytes * 131072) <= postedFile.ContentLength)
                    {
                        message = "FileSizeToBig";
                        haserror = true;
                       
                    }
                    if (haserror == false)
                    {
                        Byte[] fileBytes = new Byte[postedFile.ContentLength];
                       attch.FileBytes= postedFile.InputStream.Read(fileBytes, 0, postedFile.ContentLength);
                        string tempFilename = attchser.SaveTemporaryFile(fileBytes);
                        uploadedFileResults.Add(new UploadedFileResult() { Filename = tempFilename });
                        message = String.Format("Sucessfully uploaded {0} file(s)", Request.Files.Count);
                        attch.OriginalFilename = postedFileKey.FileName;
                        attch.LocalFilename = tempFilename;
                        attch.DateCreated = DateTime.Now;
                        attch.DateModified = DateTime.Now;
                        attch.CreatedByUserId = Convert.ToInt32(Session["UserId"]);
                        attch.ModifiedByUserId = Convert.ToInt32(Session["UserId"]);
                        attch.IsDeleted = false;

                        api.SaveAttachment(attch);

                    }
                    
                }
            }
            return Json(new { Success = true, Message = message, Files = uploadedFileResults }, JsonRequestBehavior.DenyGet);
            

           
  
            
            //foreach (var item in filecabinate)
            //{
            //    if (item != null && item.ContentLength > 0)
            //    {
            //        string url = string.Empty;
            //        string fileName = Guid.NewGuid().ToString();
            //        string path = string.Empty;
            //        path = Path.Combine(Server.MapPath("~/Img"), fileName.ToString() + "." + Path.GetExtension(item.FileName).Substring(1));
            //        item.SaveAs(path);
            //        url = fileName + "." + Path.GetExtension(item.FileName).Substring(1);
            //    }
            //}
            //return Json(new { Success = "true", Message = "File Uploded." }, JsonRequestBehavior.DenyGet);
        }
        #endregion
    }
}