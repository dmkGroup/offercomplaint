﻿using Offer.Models.Account;
using System;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Web.Security;
using Offer.Api;
using Offer.Core;
using Offer.Logic;
using Offer.Data;

namespace OfferManagement.Mvc.Controllers
{
    public class AccountController : Controller
    {
        OfferApiController api = new OfferApiController();
        AccountService accser = new AccountService();

        // GET: Account
        [AllowAnonymous()]
        public ActionResult LogIn(string returnUrl)
        
        {
            LogInView model = new LogInView();
            return View(model);
        }

        
        [HttpPost]
        [AllowAnonymous()]
        public ActionResult LogIn(LogInView model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            User user = api.GetUserByEmail_Password(model.Email, model.Password);
            if (user != null)
            {
                Authentication.SignIn(user.Email, user.UserId.ToString(), (int?)user.OrganizationId, user.Firstname, user.Lastname, Request.GetOwinContext(), model.RememberMe);

                Session["UserId"] = user.UserId;
                Session["FirstName"] = user.Firstname;
                Session["LastName"] = user.Lastname;
                return Redirect(GetRedirectUrl(model.ReturnUrl));
            }
            else
            {
                // user authN failed
                ModelState.AddModelError("", "Invalid email or password");
                return View(model);
            }
        }


        private string GetRedirectUrl(string returnUrl)
        {
            if (string.IsNullOrEmpty(returnUrl) || !Url.IsLocalUrl(returnUrl))
            {
                return Url.Action("Search", "Listing");
            }

            return returnUrl;
        }

        public ActionResult Logout()
        {
            Authentication.Logout(Request.GetOwinContext());
            Session.Clear();
            Session.Abandon();

            return Redirect("/Account/Login?Logout=1");
        }
        public ActionResult UserRegistration()
        {
            return View();
        }
        [HttpPost]
        public ActionResult UserRegistration(ProfileData model, HttpPostedFileBase UserImg)
        {
            if(ModelState.IsValid)
            {
                User data = new User();
                data.Firstname = model.Firstname;
                data.Lastname = model.Lastname;
                data.Password = model.Password;
                data.LicenseNumber = model.LicenseNumber;
                data.Email = model.Email;
                data.CompanyName = model.CompanyName;
                data.PhoneNumber = model.PhoneNumber;
                data.Enabled = true;
                data.DateCreated = System.DateTime.Now;
                data.DateModified = System.DateTime.Now;
                data.IsPrimaryAccount=true;
                data.OrganizationId = 1;
                api.SaveUser(data);
                return RedirectToAction("Login");
            }
            else
            {
                return PartialView(model);
            }
            
         
            
        }

        [AllowAnonymous()]
        public ActionResult ForgotPassword()
        {
            return View();
        }


        [AllowAnonymous()]
        [HttpPost]
        public ActionResult ForgotPassword(ForgotPasswordModel model)
        {
            if (!ModelState.IsValid)
            {
                // user authN failed
                ModelState.AddModelError("", "Invalid Email");
                return View();
            }
            else
            {
                User user = accser.GetUser(model.Email);
                
                if (user != null)
                {
                    //Rendom password for 
                    string token = Membership.GeneratePassword(12, 1);
                    
                    //Token Expiration time is  4 Hours more then mail sending time 
                    DateTime expDate = DateTime.Now.AddHours(4);
                    user.PasswordResetToken = token;
                    user.PasswordResetTokenExpire = expDate;
                    api.EditUser(user);

                //    EmailService emailService = new EmailService(CurrentUser);
                //    ForgotPasswordTemplate emailTemplate =  emailService.GetEmailTemplateDefinition<ForgotPasswordTemplate>();

                //    emailTemplate.ResetUrl = String.Format("{0}{1}{2}", WebConfig.RootUrl, "/Account/PasswordReset/?token=", token);
                //    emailTemplate.ExpirationHours = "4";
                //    emailTemplate.EmailFooter = emailService.GetPopulateFooterTemplate(null);
                //    emailTemplate.EmailHeader = emailService.GetPopulateHeaderTemplate(null);

                //    emailService.SendEmailByTemplate(emailTemplate, new System.Net.Mail.MailAddress(user.Email, FormatHelper.FormatFullName(user.Firstname, user.Lastname)), true, null);
                //
                }
                return View();
            }
        }

        [AllowAnonymous()]
        [HttpGet]
        public ActionResult PasswordReset(string token)
        {
            User user = null;

            if (!String.IsNullOrWhiteSpace(token))
            {
                user = accser.GetUserByResetToken(token);    
            }
            
            if(user == null)
            {
                ViewBag.Message = "Invalid Token. <a href=\"/Account/ForgotPassword\">Click Here</a> to get a new reset token.";
            }

            ResetPasswordModel model = new ResetPasswordModel();
            model.PasswordResetToken = token;

            return View(model);
        }

        [AllowAnonymous()]
        [HttpPost]
        public ActionResult PasswordReset(ResetPasswordModel model)
        {
            if (ModelState.IsValid)
            {
                User user = accser.GetUserByResetToken(model.PasswordResetToken);

                if (user != null)
                {
                    accser.SetPassword(user.UserId, model.NewPassword);
                }
                else
                {
                    ViewBag.Message = "Invalid Token.";
                }
            }

            return View(model);
        }

        public ActionResult _LogInModal()
        {

            LogInView model = new LogInView();
            return View(model);

        }
    }
}