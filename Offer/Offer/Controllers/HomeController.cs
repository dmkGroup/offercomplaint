﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OfferManagement.Mvc.Controllers
{    
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            if (Session["UserId"] != null)
            {
                return Redirect("~/Listing/Search");
            }
            else
            {
                return Redirect("~/Account/Login");
            }
        }

        public ViewResult Blank()
        {
            return View();
        }

    }
}