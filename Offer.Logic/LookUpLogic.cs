﻿using Offer.Communication;
using Offer.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offer.Logic
{
    public class LookUpLogic
    {
        DatabaseRepository<lookup_AttachmentType> dbattch = new DatabaseRepository<lookup_AttachmentType>();
        DatabaseRepository<lookup_BusinessType> dbbsns = new DatabaseRepository<lookup_BusinessType>();
        DatabaseRepository<lookup_EmailTemplateType> dbem = new DatabaseRepository<lookup_EmailTemplateType>();
        DatabaseRepository<lookup_ListingStatus> dbls = new DatabaseRepository<lookup_ListingStatus>();
        DatabaseRepository<lookup_LoanType> dblt = new DatabaseRepository<lookup_LoanType>();
        DatabaseRepository<lookup_OfferStatus> dbos = new DatabaseRepository<lookup_OfferStatus>();
        DatabaseRepository<geo_State> dbGEO = new DatabaseRepository<geo_State>();
        public List<lookup_ListingStatus> GetListingStatus()
        {

            List<lookup_ListingStatus> allstatus = dbls.Table.ToList();
            return allstatus;

        }
        public List<lookup_LoanType> GetLoanTypeStatus()
        {

            List<lookup_LoanType> allstatus = dblt.Table.ToList();
            return allstatus;

        }
        public List<lookup_OfferStatus> GetOfferStatus()
        {
            List<lookup_OfferStatus> allstatus = dbos.Table.ToList();
            return allstatus;

        }

        public List<lookup_AttachmentType> GetAttachmentType()
        {

            List<lookup_AttachmentType> allstatus = dbattch.Table.ToList();
            return allstatus;

        }
        public lookup_AttachmentType GetAttachmentTypeDetail(int Id)
        {

            lookup_AttachmentType data = dbattch.Table.Where(x => x.Id == Id).FirstOrDefault();
            return data;

        }
        public List<lookup_BusinessType> GetBusinessType()
        {

            List<lookup_BusinessType> allstatus = dbbsns.Table.ToList();
            return allstatus;

        }
        public lookup_BusinessType GetBusinessTypeDetail(int Id)
        {

            lookup_BusinessType allstatus = dbbsns.Table.Where(x => x.Id == Id).FirstOrDefault();
            return allstatus;

        }

        public List<geo_State> GetAllStates()
        {
            
                List<geo_State> states = dbGEO.Table.ToList();
                return states;
            
        }


    }
}
