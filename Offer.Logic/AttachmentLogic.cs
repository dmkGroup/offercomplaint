﻿using Offer.Communication;
using Offer.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offer.Logic
{
   public class AttachmentLogic
    {
       DatabaseRepository<Attachment> dbattach = new  DatabaseRepository<Attachment>();

       public void SaveAttachment(Attachment model)
       {

           dbattach.Insert(model);
           dbattach.SaveChanges();
          

       }
       public void EditAttachment(Attachment model)
       {

           dbattach.Update(model);
           dbattach.SaveChanges();


       }
        public List<Attachment> GetAttchment()
        {
           
                List<Attachment> Attch = dbattach.Table.ToList();
                return Attch;
            
        }
        public Attachment GetListingAttachment(int ListingId)
        {
           
                Attachment Attch = dbattach.Table.Where(x => x.BusinessId == ListingId && x.BusinessType_LookupId == "Listing" && x.AttachmentType_LookupId == 2).FirstOrDefault();
                return Attch;
            
        }
        public List<Attachment> GetOfferAttachment(int OfferId)
        {
           
                List<Attachment> Attch = dbattach.Table.Where(x => x.BusinessId == OfferId && x.BusinessType_LookupId == "Offer").ToList();
                return Attch;
            
        }
        public Attachment GetProfileAttachment(int UserId)
        {
           
                Attachment Attch = dbattach.Table.Where(x => x.BusinessId == UserId && x.BusinessType_LookupId == "Profile" && x.AttachmentType_LookupId == 3).FirstOrDefault();
                return Attch;
        }
    }
   
    
}
