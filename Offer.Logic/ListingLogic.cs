﻿using Offer.Communication;
using Offer.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offer.Logic
{
    public class ListingLogic
    {
        DatabaseRepository<Listing> db = new DatabaseRepository<Listing>();

        public List<Listing> SearchListings()
        {

            List<Listing> listings = db.Table.ToList();
            return listings;

        }
        public List<Listing> ListingByPrefix()
        {

            List<Listing> listings = db.Table.Where(x => x.Address != "expired" && x.Status_LookupCode != "cancelled").ToList();
            return listings;

        }
        public Listing GetListingById(int id)
        {

            Listing listing = db.Table.Where(l => l.ListingId == id).SingleOrDefault();
            return listing;

        }
        public Listing GetListingByUniqueId(string uniqueId)
        {
            Listing listing = db.Table.Where(l => l.UniqueId == uniqueId).SingleOrDefault();
            return listing;

        }
        public List<Listing> GetListingByAddress(string Address)
        {

            List<Listing> listing = db.Table.Where(l => Address.Contains(l.Address) || Address.Contains(l.State) || Address.Contains(l.City)).ToList();
            return listing;

        }
        public List<Listing> GetListingByStatus(string Status)
        {
            List<Listing> result = new List<Listing>();
            try
            {
                result = db.Table.Where(x=>x.Status_LookupCode.Equals(Status)).ToList<Listing>();
                return result;
            }
            catch (Exception)
            {

                throw;
            }
        }
        public void ChangeListingStatus(int Id, string Status)
        {

            Listing listing = db.Table.Where(l => l.ListingId == Id).SingleOrDefault();
            listing.Status_LookupCode = Status;
            db.SaveChanges();
        }
        public int  SaveListing(Listing Model)
        {
            db.Insert(Model);
            db.SaveChanges();
            return Model.ListingId;
        }
        public void EditListing(Listing Model)
        {
            db.Update(Model);
            db.SaveChanges();
        }
    }
}
