﻿using Offer.Communication;
using Offer.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offer.Logic
{
    public class OfferLogic
    {
        DatabaseRepository<OfferGroup> dboffgrp = new DatabaseRepository<OfferGroup>();
        DatabaseRepository<Offer.Data.Offer> dboff = new DatabaseRepository<Offer.Data.Offer>();
        DatabaseRepository<OfferGroupSummary> db = new DatabaseRepository<OfferGroupSummary>();
        public List<Offer.Data.Offer> GetAllOffers()
        {

            List<Offer.Data.Offer> Offers = dboff.Table.ToList();
            return Offers;

        }
        public Offer.Data.Offer GetOfferById(int id)
        {

            Offer.Data.Offer Offer = dboff.Table.Where(l => l.OfferId == id).SingleOrDefault();
            return Offer;

        }
        public void SaveOffer(Offer.Data.Offer Offer)
        {
            try
            {
                dboff.Insert(Offer);
                dboff.SaveChanges();

            }
            catch (Exception ex)
            {

            }
        }
        public List<Offer.Data.Offer> GetOfferByStatus(string Status)
        {

            List<Offer.Data.Offer> Offer = dboff.Table.Where(l => l.OfferStatus_LookupCode == Status).ToList();
            return Offer;

        }
        public void ChangeStatus(int Id, string Status)
        {

            Offer.Data.Offer Offer = dboff.Table.Where(l => l.OfferId == Id).SingleOrDefault();
            Offer.OfferStatus_LookupCode = Status;
            dboff.SaveChanges();

        }


        public List<OfferGroupSummary> OfferGroupSummarySearch(int listingId)
        {

            List<OfferGroupSummary> offerGroupSummaries = db.Table.Where(o => listingId == o.ListingId).ToList();
            return offerGroupSummaries;

        }
        public List<OfferGroupSummary> BuyerGroupOffers(int BuyerGroupId)
        {

            List<OfferGroupSummary> offerGroupSummaries = db.Table.Where(o => BuyerGroupId == o.BuyerGroupId).ToList();
            return offerGroupSummaries;

        }

        public void OfferGroupSave(OfferGroup offerGroup)
        {

            if (offerGroup.OfferGroupId == 0)
            {
                offerGroup.DateCreated = DateTime.Now;

            }

            offerGroup.DateModified = DateTime.Now;
            dboffgrp.Insert(offerGroup);
            dboffgrp.SaveChanges();
        }




        public List<OfferGroup> GetAllOfferGroup()
        {

            List<OfferGroup> ofergroup = dboffgrp.Table.ToList();
            return ofergroup;

        }
        public OfferGroup GetOfferGroupDetail(int OfferGroupId)
        {

            OfferGroup ofergroup = dboffgrp.Table.Where(x => x.OfferGroupId.Equals(OfferGroupId)).FirstOrDefault();
            return ofergroup;

        }
        public List<OfferGroupSummary> GetOfferGroupByUniqueId(Guid UniqueId)
        {

            List<OfferGroupSummary> ofergroup = db.Table.Where(x => x.UniqueId.Equals(UniqueId)).ToList();
            return ofergroup;

        }
        public List<OfferGroup> GetOfferGroupByListing(int ListingId)
        {

            List<OfferGroup> ofergroup = dboffgrp.Table.Where(x => x.Listing.Equals(ListingId)).ToList();
            return ofergroup;

        }
        public List<Offer.Data.Offer> GetOfferByGroup(int Id)
        {
            try
            {
                List<Offer.Data.Offer> offer = dboff.Table.Where(x => x.OfferGroupId == Id).ToList();
                return offer;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }


        }
        public Offer.Data.Offer GetOfferDetail(int Id)
        {
            try
            {

                Offer.Data.Offer offer = dboff.Table.Where(x => x.OfferId == Id).FirstOrDefault();
                return offer;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }


        }
        public void ChangeOfferStatus(int Id, string Status)
        {

            Offer.Data.Offer offer = dboff.Table.Where(l => l.OfferId == Id).SingleOrDefault();
            offer.OfferStatus_LookupCode = Status;
            //db.Listings.Create();
            db.SaveChanges();

        }
        /* Edit Offer Note */
        public void EditOfferNote(Offer.Data.Offer offer)
        {

            Offer.Data.Offer off = dboff.Table.Where(l => l.OfferId == offer.OfferId).SingleOrDefault();
            off.InternalNote = off.InternalNote;
            db.SaveChanges();

        }
    }
}
