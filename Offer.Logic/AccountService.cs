﻿using Offer.Communication;
using Offer.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offer.Logic
{
   public class AccountService
    {
       DatabaseRepository<User> dbUser = new DatabaseRepository<User>();
       DatabaseRepository<Registration> dbRegi = new DatabaseRepository<Registration>();
           public Registration RegisterPartialUser(Registration registration)
           {
               registration.RegistrationCode = Guid.NewGuid();
               dbRegi.Insert(registration);
               dbRegi.SaveChanges();
               return registration;
           }

           //public User ConvertRegistrationToAgent(Guid registrationCode, string nameOnCard, int expirationMonth, int expirationYear, CcTypeLookup cardType, string cardNumber, string ipAddress)
           //{
           //    cardNumber = FormatHelper.JustNumbers(cardNumber);

           //    Registration registration = this.Repo.GetRegistration(registrationCode);

           //    if (!PaymentService.ValidateCreditCardMod10Check(cardNumber))
           //    {
           //        throw new ValidationException("Invalid card number");
           //    }

           //    CcTransactionRequest ccRequest = new CcTransactionRequest();
           //    ccRequest.CcType = cardType;
           //    ccRequest.Comment1 = "Registration card number verification";
           //    ccRequest.CreditCardNumber = cardNumber;
           //    ccRequest.ExpirationMonth = expirationMonth;
           //    ccRequest.ExpirationYear = expirationYear;
           //    ccRequest.Firstname = registration.Firstname;
           //    ccRequest.Lastname = registration.Lastname;
           //    ccRequest.ProductType = ProductTypeLookup.OmsMonthly;
           //    ccRequest.RemoteIpAddress = ipAddress;
           //    ccRequest.BusinessId = registration.RegistrationId;
           //    ccRequest.BusinessType = BusinessTypeLookup.Registration;

           //    TransactionResponse transactionResponse = PaymentService.ValidateCard(ccRequest);
           //    if (!transactionResponse.Success)
           //    {
           //        throw new ValidationException("Transaction Failed: " + transactionResponse.Message);
           //    }

           //    Organization organization = new Organization();
           //    organization.Organization1 = registration.CompanyName;
           //    organization.Payment_NameOnCard = nameOnCard;
           //    organization.Payment_ExpirationMonth = expirationMonth;
           //    organization.Payment_ExpirationYear = expirationYear;
           //    organization.Payment_CardNumber = cardNumber;
           //    organization.Payment_Last4Digits = cardNumber.Substring(cardNumber.Length - 5, 4);
           //    organization.OrganizationType_LookupCode = OrganizationTypeLookup.Agent.Value;
           //    organization.Payment_CardType_LookupCode = cardType.Value;
           //    organization.Payment_Token = transactionResponse.Token;

           //    this.Repo.SaveOrganization(organization);

           //    User user = new User();
           //    user.Enabled = true;
           //    user.Email = registration.Email;
           //    user.Password = registration.Password;
           //    user.Firstname = registration.Firstname;
           //    user.Lastname = registration.Lastname;
           //    user.CompanyName = registration.CompanyName;
           //    user.OrganizationId = organization.OrganizationID;
           //    user.IsPrimaryAccount = true;

           //    this.Repo.SaveUser(user);

           //    registration.ConvertedUserId = user.UserId;
           //    registration.ConvertedDate = DateTime.Now;
           //    this.Repo.SaveRegistration(registration);

           //    return user;
           //}

           public bool EmailExists(string email)
           {
               if (this.GetUser(email.Trim()) != null)
               {
                           return true;
               }
               else return false;
           }

           public User GetUser(string email, string password)
           {
               User User = dbUser.Table.Where(l => l.Email == email.Trim() && l.Password == password).SingleOrDefault();
               return User;
           }

           public User GetUser(string email)
           {
               User User = dbUser.Table.Where(l => l.Email == email.Trim()).SingleOrDefault();
               return User;
           }

           public User GetUserByResetToken(string token)
           {
               User User = dbUser.Table.Where(l => l.PasswordResetToken == token).SingleOrDefault();
               return User;
           }

           public void SaveUser(User user)
           {
               dbUser.Insert(user);
               dbUser.SaveChanges();
           }

           public void SetPassword(int userId, string newPassword)
           {
               User user = dbUser.Table.Where(l => l.UserId == userId).SingleOrDefault();
               user.Password = newPassword; //todo: call authentication service for generation of hashed password
               user.PasswordResetToken = "";
               user.PasswordResetTokenExpire = null;
               dbUser.Update(user);
               dbUser.SaveChanges();
           }
       
    }
}
