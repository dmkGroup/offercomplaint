﻿using Offer.Communication;
using Offer.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offer.Logic
{
    public class SellerLogic
    {  DatabaseRepository<Seller> dbsell = new DatabaseRepository<Seller>();
          /* Get All Seller */
        public  List<Seller> GetAllSellerList()
        {
           
                List<Seller> Table = dbsell.Table.ToList();
                return Table;
            
        }
        /* Get Seller By SellerId */
        public  Seller GetSellerDetail(int id)
        {
            
                Seller Seller = dbsell.Table.Where(l => l.SellerId == id).SingleOrDefault();
                return Seller;
            
        }
        /* Add Seller */
        public  void SaveSeller(Seller Seller)
        {
            try
            {

                    dbsell.Insert(Seller);
                    dbsell.SaveChanges();
                
            }
            catch (Exception ex)
            {
               
                
            }
        }
        /* Get Seller By Email */
        public  Seller GetSellerByEmail(string Email)
        {
           
                Seller Seller = dbsell.Table.Where(l => l.Email.Equals(Email)).FirstOrDefault();
                return Seller;
            
        }     
        /* Edit Seller */
        public  void EditSeller(Seller Seller)
        {
             Seller sell = dbsell.Table.Where(l => l.SellerId == Seller.SellerId).SingleOrDefault();
                Seller.SellerId = Seller.SellerId;
                Seller.Firstname = Seller.Firstname;
                Seller.Lastname = Seller.Lastname;
                Seller.Email = Seller.Email;
                //Seller.State = Seller.State;
                //Seller.City = Seller.City;
                //Seller.Phone = Seller.Phone;
                //Seller.Address = Seller.Address;
                //Seller.PostalCode = Seller.PostalCode;
                Seller.DateModified = Seller.DateModified;
                Seller.ModifiedByUserId = Seller.ModifiedByUserId;
                               dbsell.SaveChanges();
            
        }
        /* Delete Seller */
        public  void DeleteSeller(int SellerId)
        {
           
                Seller Seller = dbsell.Table.Where(l => l.SellerId == SellerId).SingleOrDefault();
                dbsell.Delete(Seller);
                dbsell.SaveChanges();
            

        }
        /* Get List of Table By Listing Id */
        public List<Seller> GetSellerByListing(int ListingId)
        {
           
                List<Seller> Seller = dbsell.Table.Where(l => l.ListingId.Equals(ListingId)).ToList();
                return Seller;
            
        }
        /* Get List of Table By Name */
        public List<Seller> GetSellerByName(string Name)
        {
            
                List<Seller> Seller = dbsell.Table.Where((l => l.Firstname.Contains(Name))).ToList();
                return Seller;
            
        }    

    }
    }

