﻿using Offer.Communication;
using Offer.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offer.Logic
{
    public class BuyerLogic
    {
        DatabaseRepository<Buyer> dbbuy = new DatabaseRepository<Buyer>();
        DatabaseRepository<BuyerGroup> dbbuygrp = new DatabaseRepository<BuyerGroup>();
        /* Save New Buyer Group*/
        public void BuyerGroupSave(BuyerGroup buyerGroup)
        {

            buyerGroup.DateModified = DateTime.Now;
            dbbuygrp.Insert(buyerGroup);
            dbbuygrp.SaveChanges();

        }
        /* Get All Buyer Group */
        public List<BuyerGroup> GetAllBuyerGroup()
        {

            List<BuyerGroup> buyerGroup = dbbuygrp.Table.ToList();
            return buyerGroup;

        }

        /* Get BuyerGroup By Detail */
        public BuyerGroup GetBuyerGroupDetail(int BuyerGroupId)
        {

            BuyerGroup buyerGroup = dbbuygrp.Table.Where(l => l.BuyerGroupId == BuyerGroupId).SingleOrDefault();
            return buyerGroup;

        }
        /* Edit Buyer Group */
        public void EditBuyerGroup(BuyerGroup BuyerGroup)
        {

            BuyerGroup bg = dbbuygrp.Table.Where(l => l.BuyerGroupId == BuyerGroup.BuyerGroupId).SingleOrDefault();
            bg.BuyerGroupId = BuyerGroup.BuyerGroupId;
            bg.AgentFirstname = BuyerGroup.AgentFirstname;
            bg.AgentLastname = BuyerGroup.AgentLastname;
            bg.AgentEmail = BuyerGroup.AgentEmail;
            bg.AgentPhone = BuyerGroup.AgentPhone;
            bg.Target_Price = BuyerGroup.Target_Price;
            bg.SearchAreas = BuyerGroup.SearchAreas;
            bg.DateModified = BuyerGroup.DateModified;
            bg.ModifiedByUserId = BuyerGroup.ModifiedByUserId;
            dbbuy.SaveChanges();

        }
        /* Get All Buyer */
        public List<Buyer> GetAllBuyerList()
        {

            List<Buyer> Buyers = dbbuy.Table.ToList();
            return Buyers;

        }
        /* Get Buyer By BuyerId */
        public Buyer GetBuyerDetail(int id)
        {

            Buyer Buyer = dbbuy.Table.Where(l => l.BuyerId == id).SingleOrDefault();
            return Buyer;

        }

        /* Get Buyer By Group */
        public List<Buyer> GetBuyerByGroupId(int BuyerGroupId)
        {

            List<Buyer> Buyers = dbbuy.Table.Where(l => l.BuyerGroupId == BuyerGroupId).ToList();
            return Buyers;

        }
        /* Add Buyer */
        public void SaveBuyer(Buyer Buyer)
        {


            try
            {
                dbbuy.Insert(Buyer);
                dbbuy.SaveChanges();
            }

            catch (Exception ex)
            {
                throw;
            }
        }
        public void SaveBuyerGroup(BuyerGroup Buyer)
        {
            try
            {
                dbbuygrp.Insert(Buyer);
                dbbuy.SaveChanges();
            }

            catch (Exception ex)
            {
                throw;
            }
        }
        /* Get Buyer By Email */
        public Buyer GetBuyerByEmail(string Email)
        {

            Buyer Buyer = dbbuy.Table.Where(l => l.Email.Equals(Email)).FirstOrDefault();
            return Buyer;

        }
        /* Get Buyer By Name */
        public List<Buyer> GetBuyerByName(string Name)
        {

            List<Buyer> Buyer = dbbuy.Table.Where(l => l.Firstname.Equals(Name)).ToList();
            return Buyer;

        }
        /* Edit Buyer */
        public void EditBuyer(Buyer Buyer)
        {

            Buyer sell = dbbuy.Table.Where(l => l.BuyerId == Buyer.BuyerId).SingleOrDefault();
            Buyer.BuyerId = Buyer.BuyerId;
            Buyer.Firstname = Buyer.Firstname;
            Buyer.Lastname = Buyer.Lastname;
            Buyer.Email = Buyer.Email;
            Buyer.State = Buyer.State;
            Buyer.City = Buyer.City;
            Buyer.Phone = Buyer.Phone;
            Buyer.Address = Buyer.Address;
            Buyer.PostalCode = Buyer.PostalCode;
            Buyer.DateModified = Buyer.DateModified;
            Buyer.ModifiedByUserId = Buyer.ModifiedByUserId;
            dbbuy.SaveChanges();

        }
        /* Delete Buyer */
        public void DeleteBuyer(int BuyerId)
        {

            Buyer Buyer = dbbuy.Table.Where(l => l.BuyerId == BuyerId).SingleOrDefault();
            dbbuy.Delete(Buyer);


        }
        /* Get List of Buyers By Listing Id */
        public List<Buyer> GetBuyersByListing(int ListingId)
        {

            List<Buyer> Buyer = dbbuy.Table.Where(l => l.ListingId.Equals(ListingId)).ToList();
            return Buyer;

        }
    }
}
