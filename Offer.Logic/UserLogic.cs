﻿using Offer.Communication;
using Offer.Data;
using System.Collections.Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Offer.Logic
{
   public class UserLogic
    {
       DatabaseRepository<User> dbuser = new  DatabaseRepository<User>();
        public List<User> GetAllUserList()
        {
           
                List<User> Users = dbuser.Table.ToList();
                return Users;
            
        }
        public User GetUserDetail(int id)
        {
           
                User User = dbuser.Table.Where(l => l.UserId == id).SingleOrDefault();
                return User;
            
        }
        public void SaveUser(User User)
        {
            try
            {
                    dbuser.Insert(User);
                    dbuser.SaveChanges();
                
            }
            catch (Exception ex)
            {
               
               
            }
        }
        public User GetUserByEmail(string Email)
        {
            
                User User = dbuser.Table.Where(l => l.Email.Equals(Email)).FirstOrDefault();
                return User;
            
        }
        public User GetUserByEmail_Password(string Email, string Password)
        {

            User User = dbuser.Table.Where(l => l.Email.Equals(Email) && l.Password.Equals(Password)).FirstOrDefault();
            return User;

        }
        public List<User> GetUserByStatus(bool Status)
        {
            List<User> User = dbuser.Table.Where(l => l.Enabled.Equals(Status)).ToList();
                return User;
            
        }
        public void ChangeStatus(int Id, bool Status)
        {
           
                User User = dbuser.Table.Where(l => l.UserId == Id).SingleOrDefault();
                User.Enabled = Status;
                dbuser.SaveChanges();
            
        }
        public void ChangePassword(int Id, string Password)
        {
           
                User User = dbuser.Table.Where(l => l.UserId == Id).SingleOrDefault();
                User.Password = Password;
                dbuser.SaveChanges();
            
        }
        public void EditUser(User user)
        {
           
                User User = dbuser.Table.Where(l => l.UserId == user.UserId).SingleOrDefault();
                User.UserId = user.UserId;
                User.Firstname = user.Firstname;
                User.Lastname = user.Lastname;
                User.Email = user.Email;
                User.PhoneNumber = user.PhoneNumber;
                User.CompanyName = user.CompanyName;
                User.LicenseNumber = user.LicenseNumber;
                User.Enabled = user.Enabled;
                User.Password = user.Password;
                User.PasswordResetToken = user.PasswordResetToken;
                User.PasswordResetTokenExpire = user.PasswordResetTokenExpire;
                dbuser.SaveChanges();
            
        }
        public User GetUserByToken(string Token)
        {
           
                User User = dbuser.Table.Where(l => l.PasswordResetToken == Token).SingleOrDefault();
                return User;
            
        }
    }
}
